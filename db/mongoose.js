const mongoose = require('mongoose');
const config = require('../config.json').mongo;

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`);

module.exports = { mongoose };
