const { mongoose } = require('../mongoose');

const priceSchema = mongoose.Schema({
  time: {
    type: Number,
    default: Date.now
  },
  price: {
    type: Number,
    required: true
  }
});

// Return last price for BTC in Real (Foxbit, Bitcoin to You)
priceSchema.statics.getLastPrice = function () {
  return this.findOne({})
    .sort({ time: -1 })
    .select({ price: 1, _id: 0 })
    .exec().then((data) => {
      if(data) {
        return data.price;
      }
    }).catch(e => console.log(e));
};

priceSchema.statics.insertPrice = function (price) {
  const PriceUSD = this;
  new PriceUSD({ price }).save().catch(err => console.log('insertPrice', err));
};

const PriceUSD = mongoose.model('PriceUSD', priceSchema);

module.exports = PriceUSD;
