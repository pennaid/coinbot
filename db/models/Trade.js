const { mongoose } = require('../mongoose');
const moment = require('moment');

const tradeSchema = mongoose.Schema({
  tid: {
    type: Number,
    require: true,
    unique: true
  },
  exchange: {
    type: String,
    required: true
  },
  time: {
    type: Date,
    required: true,
    default: Date.now
  },
  type: {
    type: String,
    required: true
  },
  amount: {
    type: Number,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  author: {
    type: String,
    required: true,
    default: 'bot'
  }
});

// Acho que não vai usar isso porque vai ser 1 ordem por ordem
// tradeSchema.statics.insertManyTrades = function (trades) {
//   const Trade = this;
//
//   return Trade.insertMany(trades).catch((err) => {
//     // If duplicated key, don't log error
//     if (err.code !== 11000) {
//       console.log('insertManyTrades', err);
//     }
//   });
// };

tradeSchema.statics.updateTrade = function (trade) {
  console.log('updateTrade em Trade.js', trade);
  return this.findOneAndUpdate(
    { tid: trade.tid },
    trade,
    { new: true, upsert: true, setDefaultsOnInsert: true }
  ).exec()
  .catch(err => console.log('updateTrade', err));
};

tradeSchema.statics.getLastestTrades = function (limit) {
  return this.find({},
    'tid price amount time type')
    .select({ _id: 0 })
    .sort({ tid: -1 })
    .limit(limit)
    .exec();
};

tradeSchema.statics.getPeriodTrades = function (period) {
  const time = moment().subtract(1, period).format('x');
  return this.find({ time: { $gte: time } },
    'tid price amount time type exchange')
    .select({ _id: 0 })
    .sort({ tid: -1 })
    .exec();
}

tradeSchema.statics.getIntervalTrades = function (startStr, endStr) {
  const start = moment(startStr, "DD/MM/YYYY").format('x');
  const query = { time: { $gte: start } };
  let end;

  if (endStr) {
    end = moment(endStr, "DD/MM/YYYY").format('x');
    console.log(end);
    query.time.$lte = end;
  }

console.log(query);
  return this.find(query,
    'tid price amount time type exchange')
    .select({ _id: 0 })
    .sort({ tid: -1 })
    .exec();
}

const Trade = mongoose.model('Trade', tradeSchema);

module.exports = Trade;
