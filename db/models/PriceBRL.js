const { mongoose } = require('../mongoose');

const priceSchema = mongoose.Schema({
  time: {
    type: Number,
    default: Date.now
  },
  price: {
    type: Number,
    required: true
  }
});

// Return last quoted price for USD bitcoin
priceSchema.statics.getLastPrice = function () {
  return this.findOne({})
    .sort({ time: -1 })
    .select({ price: 1, _id: 0 })
    .exec().then((data) => {
      if(data) {
        return data.price;
      }
    });
};

priceSchema.statics.insertPrice = function (price) {
  const PriceBRL = this;
  new PriceBRL({ price }).save().catch(err => console.log('insertPrice', err));
};

const PriceBRL = mongoose.model('PriceBRL', priceSchema);

module.exports = PriceBRL;
