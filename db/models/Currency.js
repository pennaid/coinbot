const { mongoose } = require('../mongoose');

const currencySchema = mongoose.Schema({
  requestTime: {
    type: Date,
    default: Date.now
  },
  base: {
    type: String,
    required: true
  },
  assets: {}
});

currencySchema.statics.insertCurrencies = function (currencies) {
  const Currency = this;

  return new Currency(currencies).save();
}

const Currency = mongoose.model('Currency', currencySchema);

module.exports = Currency;
