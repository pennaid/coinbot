const { mongoose } = require('../mongoose');

const portfolioSchema = mongoose.Schema({
  exchange: {
    type: String,
    required: true
  },
  lastUpdate: {
    type: Number,
    require: true,
    default: Date.now
  },
  portfolio: {}
});

portfolioSchema.statics.updatePortfolio = function (exchange, portfolio) {
  return this.update(
    { exchange },
    { $set: portfolio },
    { upsert: true }
  ).catch(err => console.log(err));
};

portfolioSchema.statics.getPortfolios = function () {
  return this.find({}).select({
    _id: 0,
    exchange: 1,
    lastUpdate: 1,
    portfolio: 1
  }).catch(err => console.log(err));
};

portfolioSchema.statics.getOnePortfolio = function (exchange) {
  return this.find({ exchange }).select({
    _id: 0,
    exchange: 1,
    lastUpdate: 1,
    portfolio: 1
  }).catch(err => console.log(err));
};

const Portfolio = mongoose.model('Portfolio', portfolioSchema);

module.exports = Portfolio;
