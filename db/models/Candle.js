const { mongoose } = require('../mongoose');

const candleSchema = mongoose.Schema({
  time: {
    type: Number,
    required: true,
    unique: true
  },
  open: {
    type: Number,
    required: true
  },
  high: {
    type: Number,
    required: true
  },
  low: {
    type: Number,
    required: true
  },
  close: {
    type: Number,
    required: true
  },
  volume: {
    type: Number,
    required: true
  }
});

// Insert new candle
candleSchema.statics.insertCandle = function (time, open, high, low, close, volume) {
  const Candle = this;

  const candleData = { time, open, high, low, close, volume };

  const candle = new Candle(candleData);
  return candle.save().catch((err) => {
    // If duplicated key, don't log error
    if (err.code !== 11000) {
      console.log('insertManyCandles', err);
    }
  });
};

// Return N (limit) candles sorted by time
// { time, open, high, low, close, volume }
candleSchema.statics.getSortedCandles = function (limit) {
  return this.find({},
    'time open high low close volume')
    .select({ _id: 0 })
    .sort('time')
    .limit(limit)
    .exec();
};


// Insert several candles at once
candleSchema.statics.insertManyCandles = function (candles) {
  const Candle = this;

  return Candle.insertMany(candles).catch((err) => {
    // If duplicated key, don't log error
    if (err.code !== 11000) {
      console.log('insertManyCandles', err);
    }
  });
};

const Candle = mongoose.model('Candle', candleSchema);

module.exports = Candle;
