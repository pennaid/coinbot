const { mongoose } = require('../mongoose');

const orderSchema = mongoose.Schema({
  orderId: {
    type: Number,
    require: true
  },
  createTime: {
    type: Date,
    default: Date.now
  },
  executedTime: {
    type: Date
  },
  exchange: {
    type: String,
    required: true
  },
  action: {
    type: String,
    required: true
  },
  assets: {
    type: String,
    required: true
  },
  amount: {
    type: Number,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  status: {
    type: String,
    default: 'Pending',
    enum: ['Pending', 'Filled', 'Cancelled', 'Partially']
  }
});

// orderSchema.statics.updateOrder = function (exchange, order) {
//   this.update({ orderId: order.orderId }, {})
// };

orderSchema.statics.insertOrder = function (order) {
  const Order = this;

  const model = new Order(order);
  return model.save().catch(e => console.log(e));
};

orderSchema.statics.getOrders = function () {
  const today = new Date();
  const monthAgo = today.setMonth(today.getMonth() - 1);

  return this
  .find({ $or: [{ createTime: monthAgo }, { status: { $ne: 'Filled' } }] })
  .select({
    _id: 0,
    __v: 0
  }).catch(err => console.log(err));
};

orderSchema.statics.getOneOrder = function (orderId) {
  return this.find({ orderId })
  .select({
    _id: 0,
    __v: 0
  }).catch(err => console.log(err));
};

const Order = mongoose.model('Order', orderSchema);

module.exports = Order;
