const { mongoose } = require('../mongoose');
const _ = require('lodash');

const cardSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  exc: {
    type: String,
    required: true
  },
  mean: {
    type: Number,
    required: true,
    default: 0
  },
  spread: {
    type: Number,
    required: true,
    default: 0
  },
  buyOrderID: {
    type: String,
    required: true,
    default: 0
  },
  sellOrderID: {
    type: String,
    required: true,
    default: 0
  },
  buyOrderPrice: {
    type: Number,
    required: true,
    default: 0
  },
  sellOrderPrice: {
    type: Number,
    required: true,
    default: 0
  },
  buyOrderAmount: {
    type: Number,
    required: true,
    default: 0
  },
  sellOrderAmount: {
    type: Number,
    required: true,
    default: 0
  },
  totalBuyAmount: {
    type: Number,
    required: true,
    default: 0
  },
  totalSellAmount: {
    type: Number,
    required: true,
    default: 0
  },
  maxDiff: {
    type: Number,
    required: true,
    default: 0
  }
});

// Insert several cards at once
cardSchema.statics.insertManyCards = function (cards) {
  const Card = this;
  return Card.insertMany(cards).catch(err => console.log('insertManyCards', err));
};

cardSchema.statics.removeAllCards = function () {
  return this.remove({}).catch(err => console.log('removeAllCards', err));
}

// id: ObjectId
// update: object with modifications
cardSchema.statics.updateCard = function (id, update) {
  return this.findOneAndUpdate({ _id: id }, update, { new: true }).catch(err => console.log('updateCard', err));
}

const Card = mongoose.model('Card', cardSchema);

module.exports = Card;
