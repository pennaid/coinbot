const forever = require('forever-monitor');
const fs = require('fs');
const moment = require('moment');
const path = require('path');

const mail = require('./utils/mail');
const log = require('./utils/log');
const server = require('./interface/server');
const gSheets = require('./interface/gSheets');

// Create /logs folder if it doesn't exist
const dir = path.join(__dirname, '/logs');
if (!fs.existsSync(dir)) {
  fs.mkdirSync(dir);
};

gSheets.getPanel().then((panel) => {
  const child = new (forever.Monitor)('coinbot.js', {
    silent: false,
    uid: 'coinbot',
    minUptime: 1000,
    spinSleepTime: 1000,
    watch: false,
    logFile: 'logs/logs',
    outFile: 'logs/out',
    errFile: 'logs/errors',
    fork: true
  });

  server.init(child);

  child.on('start', function() {
    gSheets.sendStatus('ON');
  });

  child.on('exit', function () {
    log.info('Forever: Exiting Coinbot application');
    gSheets.sendStatus('OFF');
  });

  child.on('restart', function () {
    const time = moment().format('YYYY-MM-DD HH:mm:ss');
    gSheets.sendStatus('ON');

    // Send email and save log
    fs.readFile('./logs/errors', (err, data) => {
      fs.writeFile(`./logs/${time} ERR`, data);
      mail.sendMail(`${time} ERR`, data);
    });

    log.info('Restarting Forever');
  });

  child.on('stop', function () {
    log.info('Stoped Forever');
    gSheets.sendStatus('OFF');
  });

  // Error on Forever
  child.on('error', function (err) {
    const time = moment().format('YYYY-MM-DD HH:mm:ss');
    mail.sendMail(`${time} FOREVER ERR`, err);
    gSheets.sendStatus('OFF');
    log.error('FOREVER ERROR', err);
  });

  // Listen for events from the Server and send it to child process coinbot.js
  child.on('message', (data) => {
    child.send(data);
  });

  child.start();
});
