const { Broker } = require('../core/Broker');
const config = require('../config.json').dataKeeper;
// const { retrieveData } = require('../core/retrieveData');
const priceBRLGetter = require('../refreshers/priceBRLGetter');
const saveData = require('../core/saveData');

function priceBrlLoop() {
  return priceBRLGetter().then((price) => {
    saveData.insertPriceBRL(price);
    Broker.updatePriceBRL(price);
  }).catch(err => log.error('Price BRL Refresher -> getPriceBRL:', err));
}

function doLoop() {
  setInterval(priceBrlLoop, config.refreshPriceBRL);
}

module.exports = doLoop;
