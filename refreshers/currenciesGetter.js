// Cotação
// http://api.promasters.net.br/cotacao/v1/valores

// PAID
// https://currencylayer.com
// https://developer.yahoo.com/yql/console/?q=show%20tables&env=store://datatables.org/alltableswithkeys#h=select+*+from+yahoo.finance.xchange+where+pair+in+(%22CNY%22%2C+%22USD%22)


const axios = require('axios');
const _ = require('lodash');
const qs = require('querystring');
const log = require('../utils/log');
const { currency } = require('../config.json');

const query = {
  q: `select * from yahoo.finance.xchange where pair in ("${currency.assets.join('","')}")`,
  format: 'json',
  env: 'store://datatables.org/alltableswithkeys'
};

function getCurrencies() {
  return axios.get(`https://query.yahooapis.com/v1/public/yql?${qs.stringify(query)}`)
  .then((res) => {
    const data = res.data;

    const rates = data.query.results.rate;

    const assets = {};
    rates.forEach((obj) => {
      const asset = obj.Name.substr(-3);
      const rate = obj.Rate;
      assets[asset] = rate;
    });

    const currency = {
      base: 'USD',
      requestTime: new Date(data.query.created).getTime(),
      assets
    };

    return currency;
  }).catch(err => {
    log.error('Error requesting Yahoo Finance', err);

    // Fallback
    const query = {
      access_key: currency.currencyLayerKey,
      currencies: currency.assets.join(','),
      format: 1
    };

    return axios.get(`http://apilayer.net/api/live?${qs.stringify(query)}`)
    .then((res) => {
      const data = res.data;

      const rates = data.quotes;

      const assets = {};
      _.forEach(rates, (rate, key) => {
        const asset = key.substr(-3);
        assets[asset] = rate;
      });

      const currency = {
        base: 'USD',
        requestTime: data.timestamp,
        assets
      };

      return currency;
    });
  }).catch(err => log.error('Couldn\'t fetch any currency API', err));
}

module.exports = {
  getCurrencies
};
