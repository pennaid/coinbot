const { Broker } = require('../core/Broker');
const fox = require('../exchanges/get_data/FOX');
const b2u = require('../exchanges/get_data/B2U');

module.exports = function () {
  priceFox = fox.getPrice();
  priceB2U = b2u.getPrice();

  return Promise.all([priceFox, priceB2U]).then((results) => {
    /* preço medio de x bitcoins na foxbit, bit2u
    fox: media bid e ask
    b2u: media bid e ask
    media = pegar maior bid e menor ask /2
    return media
    */
    const foxBid = results[0].bid.value;
    const foxAsk = results[0].ask.value;
    const b2uBid = results[1].bid.value;
    const b2uAsk = results[1].ask.value;

    const highestBid = foxBid > b2uBid ? foxBid : b2uBid;
    const lowestAsk = foxAsk < b2uAsk ? foxAsk : b2uAsk;

    return mean = (highestBid + lowestAsk) / 2;

  }).catch(e => console.log(e));
}
