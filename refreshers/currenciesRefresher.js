const { Broker } = require('../core/Broker');
const config = require('../config.json').currency;
// const { retrieveData } = require('../core/retrieveData');
const currenciesGetter = require('../refreshers/currenciesGetter');

const nextRefresh = config.refreshInterval;

function currencyLoop() {
  const date = new Date();
  const hour = date.toLocaleString('br-PT', {hour: 'numeric',   hour12: false, timeZone: 'America/Sao_Paulo' });
  const day = date.toLocaleString('br-PT', {weekday: 'short', timeZone: 'America/Sao_Paulo' });

  if (day !== 'Sat' && day !== 'Sun' && hour >= 10 && hour <= 17) {
    // getCurrencies only if it is between 10:00 - 17:00 and Mon - Fri
    currenciesGetter.getCurrencies().then((res) => {
      const assets = {};
      const missing = [];
      const quotes = res;

      currencyConfig.assets.forEach((asset) => {
        if (!res.assets[asset]) {
          return missing.push(asset);
        }
        assets[asset] = res.assets[asset];
      });

      if (missing.length > 0) {
        missing.forEach((asset) => {
          assets[asset] = 'oi';
        });
        // busca no banco, completa assets e retorna assets
      }
      quotes.assets = assets;

      saveData.insertCurrencies(quotes).catch(e => console.log(e));
      Broker.updateCurrencies(quotes);

    }).catch(err => log.error('Currencies Refresher -> getCurrencies:', err));

    nextRefresh = config.refreshInterval;

  } else {
    nextRefresh = date.setHour(10).getTime - new Date().getTime();
    if (nextRefresh < 0) {
      nextRefresh += 24 * 60 * 60000;
    }
  }
  doLoop();
}

function doLoop() {
  setTimeout(currencyLoop, nextRefresh);
}

module.exports = doLoop;
