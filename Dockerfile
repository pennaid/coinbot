FROM node:6

#this solves build issues
ENV USER root

#ref to local node root path
ENV INSTALL_PATH /node

# so that npm installs not into the code path, so we can share this directory and still have npm_modules not shared
RUN mkdir -p $INSTALL_PATH

# so that you can `require` any installed modeule
ENV NODE_PATH $INSTALL_PATH/node_modules

# so that executable from modules are added to the path
ENV PATH $NODE_PATH/.bin:$PATH

# finaly, installs deps
COPY ./package.json $INSTALL_PATH/package.json
RUN cd $INSTALL_PATH && npm install

# initiate source
ENV SRC_PATH /src
WORKDIR $SRC_PATH
COPY . $SRC_PATH
