Coinbot

Requisitos:
  - Node.js
  - Mongodb (start mongodb rodando no console: "mongod", e deixa esse console aberto)

Quickstart

Install modules
`npm install`

Start
`npm start`

Tests
`npm tests`

Estrutura
Para manter o bot rodando, o preocesso inicia em `start.js`. Esse módulo inicia o servidor, uma conexão com o Google Sheets para manter o status do bot atualizado, e o bot.
O bot é iniciado no módulo `coinbot.js`, aqui são recuperados os dados do banco, instaciadas as exchanges e iniciado o "Broker".

/core
Arquivos de processos do COINBOT, o que faz ele funcionar.

/db
Modelos e conexão com o banco de dados.

/exchanges
Interfaces de comunicação com as exchanges.

/interface
Comunicação externa do COINBOT com o usuário. Busca e mantém conexão com Google Sheets e sobe o Servidor para receber ordens e parar/continuar o COINBOT.

/logs
Arquivos de log (informações, erros e debug).

/refreshers
Contém os módulos que ficam buscando e atualizando valores necessários para estratégias (cotações de moedas estrangeiras e preço do bitcoin BRL).

/reports
Relatórios gerados por requisição do Servidor.

/strategies
Estratégias de investimento

/tests
Testes unitários

/utils
Módulos utilitarios do sistema


Passo-a-passo da inicialização do COINBOT
O START é dado no Forever (módulo start.js), que inicia o servidor e o COINBOT.

Servidor: `interface/server.js`
O servidor tem rotas para receber ordens de compra e venda e disparar para o COINBOT, parar e reiniciar o COINBOT e pegar relatórios.

COINBOT: `coinbot.js`
O bot inicia buscando as configurações e parâmetros no Sheets (`interface/gSheets.js`), depois que pega essas informações ele inicia o Trader (`core/Trader.js`) passando por cada exchange, e recupera os dados do banco de dados. Finalizados todos esses processos, é checado se o Sheets foi recuperado, e todos os dados são passados como parâmetros para Broker (`core/Broker.js`).

Google Sheets: `interface/gSheets.js`
Busca todos os parametros necessários para o funcionamento e configuração do Bot. Também tem funções para envio do relatório de 24h para o Google Sheets e atualização do status do BOT.

Trader: `core/Trader.js`
Por onde todas as transações são realizadas
O Trader consiste num módulo que instancia as exchanges, inicia um executionStack (`core/executionStack.js`) para cada uma, e executa as ordens enviadas pelo COINBOT ou requisitadas no Servidor. Depois de executadas as ordens, retorna a resposta para a estratégia e chama Save Data (`core/saveData.js`) para salvar a resposta onde é pertinente (ordem de venda, compra, portfolio, atualização da ordem).

Execution Stack: `core/executionStack.js`
Como existe um intervalo mínimo entre as requests em cada exchange, para não enviar muitas requisições de uma vez usa-se esse módulo. Aqui as ordens são enfileiradas e executadas de acordo com o tempo mínimo entre as ordens (informado por cada exchange).

Broker: `core/Broker.js`
Gerencia o COINBOT
O Broker inicia uma instância de Data Keeper (`core/DataKeeper.js`) para persistir os parâmetros necessários pelas estratégias na memória e cria as conexões com websockets e inicia atualizações de preços das exchanges. Assim que o Data Keeper está inicializado, inicia as estratégias.
Broker também possui métodos para atualizar os dados contidos no Data Keeper.

Data Keeper: `core/DataKeeper.js`
"Guardião dos dados"
O Data Keeper mantém todos os dados necessários pelas estratégias armazenados na memória para fácil acesso.
Além de manter os dados armazedos na memória, chama Save Data (`core/saveData.js`) para salvar os dados no banco de dados.

Save Data: `core/saveData.js`
Save Data é uma interface de comunicação com os modelos do banco de dados. Repassa as informações para o modelo correto e adequa os dados ao modelo.

Utils

Email: `utils/mail.js`
Envia email quando há erro que reinicializa o COINBOT.
Defina as credenciais do GMail em `utils/config/credencials.json`.

To be able send emails using GMail from any application (including Node.js) you need to generate application-specific password to access GMail: My Account -> Sign-in & security -> Signing in to Google -> App passwords

Select 'Other (Custom name)' in 'Select app'/'Select device' drop-downs, enter descriptive name for your application and device and press 'GENERATE'. Copy provided password.

You need to have 2-step authentication on.
https://www.npmjs.com/package/gmail-send

Error Occurence: `utils/errorOccurrence.js`
Lida com todos os erros do sistema
Se houverem erros que devem paralizar o bot, basta lança-los que serão capturados em `utils/errorOccurrence` e esse módulo lidará com o processo de encerramento dos processos, lançamento do erro e finalização do bot para reinício pelo Forever.

Logs: `utils/log.js`
Formata a exibição dos logs no console.


Adicionar nova Exchange

Para adicionar uma nova exchange, basta criar um módulo que permita a comunicação do COINBOT Trader com a exchange.
Esse módulo deve ser adicionado em `exchanges/`. Se for necessário criar uma API de comunicação com a exchange, esse módulo pode ficar em `exchanges/api_wrapers`.
Configure as credenciais no `config.json`, criando um novo objeto para a exchange. Essa configuração deve ser feita localmente, pois as credenciais não devem ser enviadas para o repositório.
Para inicializar o módulo da exchange, você deve colocar o nome do módulo no `config.json`. Faça isso no Google Sheets.
Para usar o módulo, basta chamar Trader com o nome da exchange.

Requisitos
Como Trader faz as vezes da comunicação entre a estratégia e a exchange, é necessaŕio que os módulos tenham métodos obrigatórios.
Esses métodos são os métodos definidos em Trader:

sell
buy
checkOrder
cancelOrder
getPortfolio
getTrades
getOrderbook

Adicionar nova estratégia
As estratégias estão armazenadas em `strategies/`. Para adicionar uma nova estratégia basta requisitar o módulo inserido nessa pasta em `core/Broker.js`, no construtor (de preferência abaixo de `brantStrategy`).
Caso a estratégia tenha processos que precisem ser finalizados antes de reiniciar o BOT (o que muito provavelmente é o caso), faça requisição do módulo `utils/errorOccurrence.js` e envie um evento `running` quando o processo iniciar. Quando o processo finalizar envie `done`. Error Occurence vai finalizar o bot apenas quando todos os processos estiverem finalizados.
IMPORTANTE: crie um listener para `stopProcesses` que deve interromper a execução de novos processos. Esse evento é disparado quando o BOT deve ser interrompido, então é importante que todas as estratégias escutem esse evento e interrompam a execução de novas ordens, assim quando estiver tudo finalizado o BOT poderá ser interrompido.
