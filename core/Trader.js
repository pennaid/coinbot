const config = require('../config.json');
const path = require('path');
const saveData = require('./saveData');
const log = require('../utils/log');
const ExecutionStack = require('./executionStack');

// Trader.FOX...
const Trader = function () {
  config.exchanges.forEach((ex) => {
    try {
      const Trd = require(path.join('../exchanges', ex));
      Trader.prototype[ex] = new Trd(config[ex]);
      // TODO One ExecutionStack for each exchange "to rule them all"
      const executionStack = new ExecutionStack(config[ex].commandsPerSecond);
      Trader.prototype[ex].addTask = executionStack.addTask.bind(executionStack);
    } catch (err) {
      log.error(`Something wrong openning exchange ${ex}`, err);
    }
  });

  // Listen for event sent from Server (using FOREVER as intermediary) for BUY and SELL orders
  process.on('message', (data) => {
    if (data.action === 'sell') {
      this.sell(data.exchange, data.amount, data.price);
    } else if (data.action === 'buy') {
      this.buy(data.exchange, data.amount, data.price);
    }
  });
};

// This class handles the exchange orders.
// It sets a callback which returns a promise response and has a request timeout, which is triggered when the request is sent.
// Since there ARE SDK's which doesn't support promises, everything will use callbacks.
class RequestHandler {
  constructor(resolve, reject) {
    this.resolve = resolve;
    this.reject = reject;
    this.callback = this.callback.bind(this);
  }

  startCountdown () {
    this.promiseTimeout = setTimeout(() => {
      this.reject('Trader promise timeout');
    }, 5000);
  }

  callback (err, data) {
    clearTimeout(this.promiseTimeout);
    if (err) {
      data = { error: err };
    }
    this.resolve(data);
  }
}

// Send BUY order to exchange, save order and return response
Trader.prototype.buy = function (exchange, amount, price) {
  try {
    const run = function (resolve, reject) {
      const requestHandler = new RequestHandler(resolve, reject);
      this[exchange].addTask([
          this[exchange].buy.bind(this[exchange]),
          amount, price, requestHandler
      ]);
    }.bind(this);

    return new Promise(run).then((response) => {
      if(!response.error) {
        saveData.insertOrder(exchange, response);
      }
      return response;
    });
  } catch (err) {
    log.error('Trader buy CATCH ERROR:', err);
  }
};

// Send SELL order to exchange, save order and return response
Trader.prototype.sell = function (exchange, amount, price) {
  try {
    const run = function (resolve, reject) {
      const requestHandler = new RequestHandler(resolve, reject);
      this[exchange].addTask([
          this[exchange].sell.bind(this[exchange]),
          amount, price, requestHandler
      ]);
    }.bind(this);

    return new Promise(run).then((response) => {
      if(!response.error) {
        saveData.insertOrder(exchange, response);
      }
      return response;
    });
  } catch (err) {
    log.error('Trader sell CATCH ERROR:', err);
  }
};


/**
Get all portfolios. If given an exchange name, get only the exchange's portfolio
@return { BRL: 0.00570061, BRL_locked: 0, BTC: 0.00299292, BTC_locked: 0 }
*/
Trader.prototype.getPortfolio = function (exchange) {
  exchange = exchange.toUpperCase();
  try {
    const run = function (resolve, reject) {
      const requestHandler = new RequestHandler(resolve, reject);
      this[exchange].addTask([
        this[exchange].getPortfolio.bind(this[exchange]),
        requestHandler
      ]);
    }.bind(this);

    return new Promise(run).then((portfolio) => {
      saveData.updatePortfolio(exchange, portfolio);
      return portfolio;
    });
  } catch (err) {
    log.error('Trader getPortfolio CATCH ERROR:', err);
  }
};

// Check order passing the order ID (if it was executed or not)
Trader.prototype.checkOrder = function (exchange, orderId) {
  try {
    const run = function (resolve, reject) {
      const requestHandler = new RequestHandler(resolve, reject);
      this[exchange].addTask([
        this[exchange].checkOrder.bind(this[exchange]),
        orderId, requestHandler
      ]);
    }.bind(this);

    return new Promise(run).then((res) => {
      if (res && res.executedAmount > 0) {
        saveData.updateTrade(orderId, exchange, res.type, res.executedAmount, price);
      }
      return res;
    }).catch(e => log.error('Trader checkOrder error:', e));
  } catch (err) {
    log.error('Trader checkOrder CATCH ERROR:', err);
  }
};

// Cancel order with the given order ID
Trader.prototype.cancelOrder = function (exchange, orderId) {
  try {
    const run = function (resolve, reject) {
      const requestHandler = new RequestHandler(resolve, reject);
      this[exchange].addTask([
        this[exchange].cancelOrder.bind(this[exchange]),
        orderId, requestHandler
      ]);
    }.bind(this);

    return new Promise(run).catch(e => log.error('Trader cancelOrder error:', e));
  } catch (err) {
    log.error('Trader cancelOrder CATCH ERROR:', err);
  }
};

// Get trades from exchange
Trader.prototype.getTrades = function (exchange) {
  try {
    const run = function (resolve, reject) {
      const requestHandler = new RequestHandler(resolve, reject);
      this[exchange].addTask([
        this[exchange].getTrades.bind(this[exchange]),
        requestHandler
      ]);
    }.bind(this);

    return new Promise(run).catch(e => log.error('Trader getTrades error:', e));
  } catch (err) {
    log.error('Trader getTrades CATCH ERROR:', err);
  }
};

// Get all open orders (used for OKC)
// Trader.prototype.getOpenOrders = function (exchange) {
//   try {
//     const run = function (resolve, reject) {
//       const requestHandler = new RequestHandler(resolve, reject);
//       this[exchange].addTask([
//         this[exchange].getOrderbook.bind(this[exchange]),
//         requestHandler
//       ]);
//     }.bind(this);
//
//     return new Promise(run).catch(e => log.error('Trader getOrderbook error:', e));
//   } catch (err) {
//     log.error('Trader getOrderbook CATCH ERROR:', err);
//   }
// };

// Get order book from exchange
Trader.prototype.getOrderbook = function (exchange) {
  try {
    const run = function (resolve, reject) {
      const requestHandler = new RequestHandler(resolve, reject);
      this[exchange].addTask([
        this[exchange].getOrderbook.bind(this[exchange]),
        requestHandler
      ]);
    }.bind(this);

    return new Promise(run).catch(e => log.error('Trader getOrderbook error:', e));
  } catch (err) {
    log.error('Trader getOrderbook CATCH ERROR:', err);
  }
};

module.exports = { Trader };
