const Candle = require('../db/models/Candle');
const Trade = require('../db/models/Trade');
const PriceUSD = require('../db/models/PriceUSD');
const PriceBRL = require('../db/models/PriceBRL');
const Currency = require('../db/models/Currency');
const Portfolio = require('../db/models/Portfolio');
const Order = require('../db/models/Order');
const config = require('../config.json');
const _ = require('lodash');

const saveData = {
  insertPriceUSD(price) {
    return PriceUSD.insertPrice(price);
  },

  insertPriceBRL(price) {
    return PriceBRL.insertPrice(price);
  },

  insertManyCandles(candles) {
    return Candle.insertManyCandles(candles);
  },

  insertCandle(time, open, high, low, close, volume) {
    return Candle.insertCandle(time, open, high, low, close, volume);
  },

  insertManyTrades(trades) {
    return Trade.insertManyTrades(trades);
  },

  updateTrade(tid, exchange, type, amount, price, time, author) {
    const trade = {
      tid, exchange, type, time, amount, price, author
    }
    return Trade.updateTrade(trade);
  },

  insertCurrencies(currencies) {
    return Currency.insertCurrencies(currencies);
  },

  updatePortfolio(exchange, portfolio) {
    const currency = config[exchange].currency;
    const asset = config[exchange].asset;
    const filtered = {};

    filtered[currency] = portfolio[currency];
    filtered[currency + '_locked'] = portfolio[currency + '_locked'];
    filtered[asset] = portfolio[asset];
    filtered[asset + '_locked'] = portfolio[asset + '_locked'];

    const exchangePort = {
      exchange,
      portfolio: filtered
    };

    return Portfolio.updatePortfolio(exchange, exchangePort);
  },

  insertOrder(exchange, order) {
    const model = _.assign(order, { exchange });

    return Order.insertOrder(model);
  }

};

module.exports = saveData;
