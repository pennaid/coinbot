/**
* This is an Execution Stack of the requests to be sent to the exchanges.
* @param commandsPerSecond tells how many requests per second the exchange accepts
* For each request, a task must be added as [method, param1, param2...]
*/
class ExecutionStack {
  constructor (commandsPerSecond) {
    this.executionList = [];
    this.interval = 1000 / commandsPerSecond;
  }

  // Start looping through tasks. Loop is stoped when there is no tasks queued.
  startLoop () {
    this.loop = setInterval(() => {
      this.executeTask();
    }, this.interval);
  }

  // Add order to stack
  addTask (task) {
    this.executionList.push(task);
    if (!this.loop) {
      this.startLoop();
    }
  }

  // Execute first order in the line. It start order and start request timeout
  executeTask() {
    const task = this.executionList.shift();
    if (task) {
      // Method is the first element
      const method = task.shift();

      // Get requestHandler: set callback as param for task and start request timeout
      const requestHandler = task.pop();
      task.push(requestHandler.callback);
      requestHandler.startCountdown();

      // Run exchange method
      method(...task);

    } else {
      // If there is no task lined, stop loop
      clearInterval(this.loop);
      this.loop = null;
    }
  }
}

module.exports = ExecutionStack;
