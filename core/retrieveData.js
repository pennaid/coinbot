const Candle = require('../db/models/Candle');
const Trade = require('../db/models/Trade');
const PriceUSD = require('../db/models/PriceUSD');
const PriceBRL = require('../db/models/PriceBRL');
const Currency = require('../db/models/Currency');
const Portfolio = require('../db/models/Portfolio');
const Order = require('../db/models/Order');

const saveData = require('./saveData');
const currencyConfig = require('../config.json').currency;
const config = require('../config.json');

const retrieveData = {};

retrieveData.getCandles = () => Candle.getSortedCandles(config.dataKeeper.candlesToStore);

retrieveData.getTrades = () => Trade.getLastestTrades(config.dataKeeper.tradesToStore);

retrieveData.getPeriodReport = (period) => Trade.getPeriodTrades(period);

retrieveData.getIntervalReport = (start, end) => Trade.getIntervalTrades(start, end);

retrieveData.getPortfolios = (trader) => {
  const promiseArray = [];
  for (exchanges of config.exchanges) {
    promiseArray.push(trader.getPortfolio(exchanges));
  }

  return Promise.all(promiseArray).then((results) => {
    return {
      FOX: results[0],
      B2U: results[1]
    }
  });
};


// FOR SHEETS
// Get orders. If an orderId is given, filter the order
retrieveData.getOrders = (orderId) => {
  if (orderId) {
    return Order.getOneOrder(orderId);
  }
  return Order.getOrders();
};

module.exports = { retrieveData };
