const { DataKeeper } = require('./DataKeeper');
const Brant = require('../strategies/Brant/Brant');
const Hoff = require('../strategies/Hoff/Hoff');
const log = require('../utils/log');

let dataKeeper;
let brantStrategy;

const Broker = function ({ priceBRL, candles, trades, currencies, portfolios, sheets }, trader ) {

  dataKeeper = new DataKeeper(priceBRL, candles, trades, currencies, portfolios);
  require('./connections');
  require('../refreshers/currenciesRefresher')();

  // When data keeper is ready, start the strategies
  dataKeeper.once('startStrategies', () => {
    log.info('Start strategies.');
    brantStrategy = new Brant(dataKeeper, sheets, trader);
    hoffStrategy = new Hoff(dataKeeper, trader);
  });

};

// Update Data Keeper with new data
Broker.newTicker = function (exchange, data) {
  const ex = exchange.toLowerCase();
  const method = `newTicker_${ex}`;

  dataKeeper[method](data);
};

// Update Data Keeper with new data
Broker.newCandle = function (exchange, data) {
  const ex = exchange.toLowerCase();
  const method = `newCandle_${ex}`;

  dataKeeper[method](data);
};

// Update Data Keeper with new data
Broker.newTrade = function (exchange, data) {
  const ex = exchange.toLowerCase();
  const method = `newTrade_${ex}`;

  dataKeeper[method](data);
};

// Update Data Keeper with new data
Broker.updateCurrencies = function (currencies) {
  dataKeeper.refreshCurrencies(currencies);
};

// Update Data Keeper with new data
Broker.updatePriceBRL = function (priceBRL) {
  dataKeeper.refreshPriceBRL(priceBRL);
};

module.exports = { Broker };
