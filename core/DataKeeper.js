const config = require('../config.json').dataKeeper;
const _ = require('lodash');
const EventEmitter = require('events');
const util = require('util');
const utils = require('../utils/util');
const saveData = require('./saveData');

class DataKeeper {
  constructor(priceBRL, candles, trades, currencies, portfolios) {
    this.priceBRL = priceBRL;
    this.candles = candles;
    this.trades = trades;
    this.portfolios = portfolios;
    this.currencies = currencies;

    // Set PriceUSD when there is data from exchange's tickers.
    // While there is no data, don't start strategies.
    this.priceUSD = {};
    const priceUSDInterval = setInterval(() => {
      if (Object.keys(this.priceUSD).length !== 0) {
        setInterval(this.setPriceUSD.bind(this), config.refreshPriceUSD);
        clearInterval(priceUSDInterval);
        // Tell Broker to start strategies
        this.emit('startStrategies');
      }
    }, 1000);

  }

  getCandles() {
    return this.candles;
  }

  // Set Bitcoin price (in USD) calculated with the exchange's prices mean
  setPriceUSD() {
    let i = 0;
    let calc = 0;

    _.forOwn(this.priceUSD, (obj, key) => {
      if (key !== 'price') {
        // If price was updated in at least the last 2 minutes
        if (obj.time > Date.now() - (2 * 120000)) {
          calc += +obj.price;
          i++;
        }
      }
    });

    if (i) {
      this.priceUSD.price = calc / i;
      return saveData.insertPriceUSD(calc / i);
    }
  }

  // Insert new candle sent by OK Coin websokect connection
  newCandle_okcoin(json) {
    const array = json[0].data;
    const candleArray = [];
    let candlePromise;

    // If returned data has more than 1 candle, insertMany
    if (_.isObject(array[0])) {
      array.forEach((candle) => {
        const time = candle[0];
        const open = candle[1];
        const high = candle[2];
        const low = candle[3];
        const close = candle[4];
        const volume = candle[5];

        this.candles.push({ time, open, high, low, close, volume });

        candleArray.push({ time, open, high, low, close, volume });
      });

      candlePromise = saveData.insertManyCandles(candleArray);
    } else {
      // Else, insert one
      const time = array[0];
      const open = array[1];
      const high = array[2];
      const low = array[3];
      const close = array[4];
      const volume = array[5];

      this.candles.push({ time, open, high, low, close, volume });

      candlePromise = saveData.insertCandle(time, open, high, low, close, volume);
    }

    this.candles = utils.sortUniqLimit(this.candles, 'time', 'time', config.candlesToStore);

    return candlePromise;
  }

  // Update prices to be calculated in setPrices()
  newTicker_okcoin(ticker) {
    this.priceUSD.okcoin = {
      price: ticker[0].data.last,
      time: ticker[0].data.timestamp
    };
  }

  // Update prices to be calculated in setPrices()
  newTicker_bitfinex(ticker) {
    this.priceUSD.bitfinex = {
      price: ticker.lastPrice,
      time: new Date().getTime()
    };
  }

  // Update prices to be calculated in setPrices()
  newTicker_bitstamp(ticker) {
    this.priceUSD.bitstamp = {
      price: ticker.price,
      time: ticker.timestamp
    };
  }

  // Update currencies with latest data
  refreshCurrencies(currencies) {
    const assets = this.currencies.assets;
    this.currencies.requestTime = currencies.requestTime;

    // Check if delta is < 10%
    _.forEach(currencies.assets, (value, asset) => {
      if (!assets[asset] || utils.checkDiff(value, assets[asset], 0.1)) {
        assets[asset] = value;
      }
    });

    saveData.insertCurrencies(currencies);
  }

  // Update Bitcoin price (in BRL) with latest data
  refreshPriceBRL(priceBRL) {
    this.priceBRL = priceBRL;
    saveData.insertPriceBRL(priceBRL);
  }

  // Persist new trade received from OK Coin websocket
  newTrade_okcoin(trade) {
    const tradesData = trade[0].data;
    const trades = [];

    // Get each property and push object
    tradesData.forEach((obj) => {
      const tid = obj[0];
      const price = obj[1];
      const amount = obj[2];
      const time = utils.getDateFromTime(obj[3]);
      let type;

      if (obj[4] === 'bid') {
        type = 'buy';
      } else if (obj[4] === 'ask') {
        type = 'sell';
      }

      trades.push({ tid, price, amount, time, type });
    });

    this.trades = utils.sortUniqLimit(this.trades, 'tid', 'tid', config.tradesToStore);

    saveData.insertManyTrades(trades);
  }
}

util.inherits(DataKeeper, EventEmitter);
module.exports = { DataKeeper };
