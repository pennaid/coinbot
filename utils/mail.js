const gmail = require('gmail-send');
const fs = require('fs');
const moment = require('moment');
const credentials = {
  user: process.env.GMAIL_USER,
  pass: process.env.GMAIL_PASSWORD
};

module.exports.sendMail = (subject, body) => {
  gmail({
    user:    credentials.user,           // Your GMail account used to send emails
    pass:    credentials.pass,           // Application-specific password
    to:      credentials.user,           // Send to yourself
    subject: subject,
    text:    body                     // Plain text
  })();                               // Send without any check
}
