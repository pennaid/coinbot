const gSheets = require('../interface/gSheets');
const moment = require('moment');
const EventEmitter = require('events');
const log = require('./log');
const errorOccurrence = new EventEmitter();

const checkAgain = 3 * 60000; // 3 minutes to check again
let errorMsg = false;
let lastDone = new Date();
let holdProcesses = 0;

// Capture Errors not catched and start BOT reinicialization process
process.on('uncaughtException', (err) => {
  errorMsg = err;
  errorOccurrence.emit('stopProcesses');
  const time = moment().format('YYYY-MM-DD HH:mm:ss');
  log.error(`${time}\nUncaught Exception -> ${errorMsg.stack}`);
});

// Capture Promise rejections not handled and start BOT reinicialization process
process.on('unhandledRejection', (reason, p) => {
  errorMsg = `Promise: ${reason}`;
  errorOccurrence.emit('stopProcesses');
  const time = moment().format('YYYY-MM-DD HH:mm:ss');
  log.error(`${time}\nUnhandled Rejection -> ${errorMsg}`, p);
});

// Listen to strategy process ending, then check if an Error has occured to restart BOT (will only restart if there are no processes pending and there is an Error)
errorOccurrence.on('done', () => {
  holdProcesses--;
  lastDone = new Date();
  if (errorMsg && holdProcesses == 0) {
    const time = moment().format('YYYY-MM-DD HH:mm:ss');
    process.stderr.write(`${time}\n${errorMsg.stack}`);
    gSheets.sendStatus('OFF');
  }
});

// Listen for events telling there is a strategy running. When the strategy process is done, it should emit a "done" event.
errorOccurrence.on('running', () => {
  holdProcesses++;
});

setInterval(() => {
  const interval = 5 * 60000; // last executionDone event

  const now = new Date().getTime();
  // If last event to executionDone has more than 5min and error happened ("limbo"), call executionDone and restart
  if (now - lastDone.getTime() > interval && errorMsg) {
    errorOccurrence.emit('executionDone');
  }
}, checkAgain);

module.exports = {
  errorOccurrence
}
