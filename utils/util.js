const _ = require('lodash');
const moment = require('moment');

const utils = {
  // Returns sorted array, uniqued and sized by given param
  sortUniqLimit: (array, sortBy, uniqBy, limit) => {
    // Sort array
    let transformed = _.sortBy(array, sortBy);

    // Remove duplicates
    transformed = _.sortedUniqBy(transformed, uniqBy);

    // Keep only the # latest
    if (transformed.length > limit) {
      const exceed = transformed.length - limit;
      for (let i = 0; i < exceed; i++) {
        transformed.shift();
      }
    }

    return transformed;
  },

  // Returns moment date object from timestamp
  getDateFromTime: (time) => {
    return moment(time, 'HH:mm:ss').subtract(9, 'hours').valueOf();
  },

  // @param diff decimal
  checkDiff: function(current, before, diff) {
    const delta = ((current - before) / before);
    return Math.abs(delta) < diff;
  }
};


module.exports = utils;
