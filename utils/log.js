const moment = require('moment');

const log = {};

log.error = (msg, err, result) => {
  const time = moment().format('YYYY-MM-DD HH:mm:ss');
  console.log(`${time} ERROR:`, msg, err, result);
};

log.info = (...args) => {
  const time = moment().format('YYYY-MM-DD HH:mm:ss');
  console.log(`${time} INFO: ${args}`);
};

log.debug = (...args) => {
  const time = moment().format('YYYY-MM-DD HH:mm:ss');
  args.forEach((arg) => console.log(`${time} DEBUG: ${arg}`));
}

module.exports = log;
