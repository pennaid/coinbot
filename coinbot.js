require('./utils/errorOccurrence');
const sheets = require('./interface/gSheets');

sheets.initSheets().then((sheets) => {
  const { retrieveData } = require('./core/retrieveData');
  const { Trader } = require('./core/Trader');
  const { Broker } = require('./core/Broker');
  const getPriceBRL = require('./refreshers/priceBRLGetter');
  const { getCurrencies } = require('./refreshers/currenciesGetter');
  const log = require('./utils/log');
  const _ = require('lodash');

  const trader = new Trader();

  // get all info from db or web. PriceUSD is calculated with websocket, so isn't retrieved
  Promise.all([
    retrieveData.getCandles(),
    getPriceBRL(),
    retrieveData.getTrades(),
    getCurrencies(),
    retrieveData.getPortfolios(trader),
  ]).then((results) => {
    let brokerProps = {
      sheets,
      candles: results[0] || [],
      priceBRL: results[1] || 0,
      trades: results[2] || [],
      currencies: results[3] || {},
      portfolios: results[4] || {},
    };

    if (!brokerProps.sheets) {
      throw new Error('Couldn\'t fetch Cards from Google Sheets');
    }

    const broker = new Broker(brokerProps, trader);

  }).then(() => {
    log.info('Running.');
  });
}).catch(e => { throw new Error(e) });
