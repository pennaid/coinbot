const log = require('../utils/log');
const config = require('../config.json');
const { retrieveData } = require('../core/retrieveData');
const gSheets = require('./gSheets');
const json2csv = require('json2csv');
const fs = require('fs');
const path = require('path');


const app = require('express')();

let coinbotProcess;

// Initialize server with the coinbot Process from start.js. Used to START and STOP bot.
function init (coinbot) {
  coinbotProcess = coinbot;
}

// Generate CSV file in /reports folder and send it back to client
function generateCSV (tradesRes, res) {
  if (!tradesRes || !tradesRes[0]) {
    res.send('No data');
  } else {
    trades = JSON.parse(JSON.stringify(tradesRes));
    const fields = Object.keys(trades[0]);
    const csv = json2csv({ fields, data: trades });

    const dir = path.join(__dirname, '../', '/reports');
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
    }

    fs.writeFile('reports/trades.csv', csv, function(err) {
      if (err) throw err;
      res.sendFile(dir + '/trades.csv');
    });
  }
}

// Stop COINBOT process (password needed)
app.get('/stop', (req, res) => {
  if (req.query.pass && req.query.pass == config.server.password) {
    coinbotProcess.stop();
    return res.send('Coinbot Stopped');
  }

  res.status(400).send('Missing information.');
});

// Restart COINBOT process (password needed) - Triggered only if process not running already
app.get('/start', (req, res) => {
  if (req.query.pass && req.query.pass == config.server.password) {
    coinbotProcess.start();
    return res.send('Coinbot Started');
  }

  res.status(400).send('Missing information.');
});

// Get 24h report for Google Sheets
app.get('/getSheetsReport', (req, res) => {
  retrieveData.getPeriodReport('day').then((tradesRes) => {
    trades = JSON.parse(JSON.stringify(tradesRes));
    gSheets.updateTradeSheet(trades).then(() => {
      res.send('Sheets updated');
    });
  }).catch(err => log.error('server /getSheetsReport', err));
});


// Get trades report with START and END date. START is required (might be 23/04 or 23/04/2017 with year)
app.get('/getReport', (req, res) => {
  const time = req.query.time;
  const start = req.query.start;
  const end = req.query.end;

  if (time) {
    retrieveData.getPeriodReport(time).then((tradesRes) => {
      generateCSV(tradesRes, res);
    }).catch(err => log.error('server /getReport', err));
  } else if (start || (start && end)) {
    retrieveData.getIntervalReport(start, end).then((tradesRes) => {
      generateCSV(tradesRes, res);
    }).catch(err => log.error('server /getReport', err));
  } else {
    res.status(400).send('Missing information.');
  }
});

// Send order to Trader, using Forever as intermediary: emit message event to Forever, which emit it forward to COINBOT
app.get('/:action', (req, res, next) => {
  const action = req.params.action;

  if (action === 'sell' || action === 'buy') {
    const exchange = req.query.exchange;
    const price = req.query.price;
    const amount = req.query.amount;

    const data = {
      action,
      exchange,
      price,
      amount
    }

    coinbotProcess.emit('message', data);
  } else {
    next();
  }

});

app.use((req, res) => {
  res.status(404).send('Not found');
});

app.listen(4004, () => {
  log.info('Server listening on port 4004.');
});

module.exports = {
  app,
  init
}
