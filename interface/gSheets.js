const GoogleSpreadsheet = require('google-spreadsheet');
const fs = require('fs');
const _ = require('lodash');
const log = require('../utils/log');

const doc = new GoogleSpreadsheet('17G_bVFY5bDhIObsX_F5ePUScc9Lg-XRTFCyf6wt7b1w');
let sheets;
let panel;
let tradesSheet;

const configJson = require('../config.json');

// Authenticate user on Google Sheets
const authenticate = function() {
  return new Promise((resolve, reject) => {
    var creds = require('./authentication.json');
    doc.useServiceAccountAuth(creds, resolve);
  });
}


const initSheets = function () {
  return authenticate().then(() => {
    return new Promise((resolve) => {
      // Get worksheets information from Google Sheets
      doc.getInfo(function(err, info) {

        log.info('Get sheets');

        cardSheet = info.worksheets[1];
        exchangeSheet = info.worksheets[3];
        configSheet = info.worksheets[4];

        sheets = {
          cardSheet,
          exchangeSheet,
          configSheet
        };

        resolve();
      });
    });
  }).then(() => {
    return new Promise((resolve) => {
      // Get cards parameters
      sheets.cardSheet.getRows({}, (err, cards) => {
        if (err) { throw new Error('Couldn\'t get cards.') }

        log.info('Set Cards');

        cards.forEach((card) => {
          card.mean = card._mean = +card.mean;
          card.spread = card._spread = +card.spread;

          card.sellOrderID = card.sellorderid;
          card.buyOrderID = card.buyorderid;

          card.buyOrderPrice = +card.buyorderprice;
          card.sellOrderPrice = +card.sellorderprice;
          card.buyOrderAmount = +card.buyorderamount;
          card.sellOrderAmount = +card.sellorderamount;
          card.totalBuyAmount = +card.totalbuyamount;
          card.totalSellAmount = +card.totalsellamount;
          card.maxDiff = +card.maxdiff;

          delete card.buyorderid;
          delete card.sellorderid;
          delete card.buyorderprice;
          delete card.sellorderprice;
          delete card.buyorderamount;
          delete card.sellorderamount;
          delete card.totalbuyamount;
          delete card.totalsellamount;
          delete card.maxdiff;
        });

        sheets.cards = cards;
        resolve();
      });
    });
  }).then(() => {
    return new Promise((resolve) => {
      // Get parameters for exchanges
      sheets.exchangeSheet.getRows({
        'max-col': 5,
      }, (err, exchanges) => {

        log.info('Set Exchanges');

        exchanges.forEach((exchange) => {
          _.assign(configJson[exchange.name], {
            currency: exchange.currency,
            asset: exchange.asset,
            commandsPerSecond: +exchange.commandsec,
            fee: +exchange.fee
          });
        });
      });
      resolve();
    });
  }).then(() => {
    return new Promise((resolve) => {
      // Get configuration parameters
      sheets.configSheet.getCells({
        'return-empty': true
      }, (err, configs) => {
        log.info('Set Config');

        // Last column on Google Sheets
        const lastColumn = 'r';

        // Given letter, returns column as number
        function calcColumn (column) {
          let col = 0;
          let mult = 1;
          column = column.toLowerCase();
          column.split().forEach((letter) => {
            col += (letter.charCodeAt(0) - 96) * mult;
            mult = 26;
          });

          return col;
        }

        function position (column, row) {
          if (typeof column == 'string'){
            column = calcColumn(column);
          }
          const lastCol = calcColumn(lastColumn);

          return ((row - 1) * lastCol) + column - 1;
        }

        const config = {
          B2C: {
            key: process.env.B2C_KEY,
            secret: process.env.B2C_SECRET
          },
          FOX: {
            "key": process.env.FOX_KEY,
            "secret": process.env.FOX_SECRET,
            "password": process.env.FOX_PASSWORD
          },
          OKC: {
            "key": process.env.OKC_KEY,
            "secret": process.env.OKC_SECRET
          },
          exchanges: configs[position('B', 5)].value.split(','),
          dataKeeper: {
            candlesToStore: configs[position('B', 2)]._numericValue,
            tradesToStore: configs[position('B', 3)]._numericValue,
            refreshPriceUSD: configs[position('F', 2)]._numericValue,
            refreshPriceBRL: configs[position('F', 3)]._numericValue,
            BTCsToConsider: configs[position('B', 4)]._numericValue
          },
          currency: {
            assets: configs[position('O', 2)].value.split(','),
            refreshInterval: configs[position('F', 4)]._numericValue
          },
          cards: {
            refreshTime: configs[position('F', 5)]._numericValue
          },
          brant: {
            minimumSell: configs[position('I', 2)]._numericValue,
            minimumBuy: configs[position('I', 3)]._numericValue,
            target: configs[position('I', 4)]._numericValue,
            macd: {
              short: configs[position('I', 5)]._numericValue,
              long: configs[position('I', 6)]._numericValue,
              limit: configs[position('I', 7)]._numericValue,
            }
          },
          hoff: {
            refreshPosition: configs[position('F', 6)]._numericValue,
            rsiPeriods: configs[position('L', 2)]._numericValue,
            priceMargin: configs[position('L', 3)]._numericValue,
            candlePeriod: configs[position('L', 7)]._numericValue,
            macd: {
              short: configs[position('L', 4)]._numericValue,
              long: configs[position('L', 5)]._numericValue,
              limit: configs[position('L', 6)]._numericValue,
            }
          },
          server: {
            password: process.env.SERVER_PASSWORD
          }
        }

        configJson.exchanges = config.exchanges;
        _.assign(configJson.dataKeeper, config.dataKeeper);
        _.assign(configJson.currency, config.currency);
        _.assign(configJson.cards, config.cards);
        _.assign(configJson.indicators, config.indicators);
        _.assign(configJson.brant, config.brant);
        _.assign(configJson.hoff, config.hoff);
        _.assign(configJson.B2C, config.B2C);
        _.assign(configJson.FOX, config.FOX);
        _.assign(configJson.OKC, config.OKC);
        _.assign(configJson.server, config.server);

        fs.writeFileSync('./config.json', JSON.stringify(configJson, undefined, 2));
        sheets.configCells = configs;
        resolve(sheets);
      });
    });
  }).catch(e => { throw new Error(e) });
}

// Get panel worksheet (used to send bot status)
const getPanel = function () {
  return authenticate().then(() => {
    return new Promise((resolve) => {
      doc.getInfo(function(err, info) {
        resolve(panelSheet = info.worksheets[0]);
      });
    });
  }).then((panelRes) => {
    return new Promise((resolve) => {
      panelRes.getCells({
        'return-empty': true
      }, (err, configs) => {
        log.info('Get panel');

        panel = configs;
        resolve(configs);
      });
    });
  })
}

// Send ON or OFF depending on bot status
sendStatus = (status) => {
  if (!panel) {
    return getPanel().then(() => {
      return sendStatus(status);
    }).catch(() => log.error('Error getting panel'));
  } else {
    statusCell = panel[1];
    statusCell.value = status;

    return statusCell.save(function (err) {
      log.info(`Sending Status: ${status}`);
      if (err) throw new Error(err);
      if (status === 'OFF') process.exit();

    }.bind(this));
  }
}

// Send 24h trades to Google Sheets (on request)
updateTradeSheet = (trades) => {

  // Clear worksheet then send the information retrieved from database
  function clearAndUpdate() {
    return new Promise((resolve) => {
      tradesSheet.clear(() => {
        tradesSheet.resize({rowCount: 1, colCount: 6}, () => {
          tradesSheet.setHeaderRow(
            ['time', 'tid', 'exchange', 'type', 'amount', 'price'],
            () => {
              trades.forEach((trade) => {
                trade.time = new Date(trade.time);
                tradesSheet.addRow(trade);
              });
              resolve();
            }
          );
        });
      });
    });
  }

  // If Trades report worksheets has already been retrieved..
  if (tradesSheet) {
    return clearAndUpdate();
  } else {
    return authenticate().then(() => {
      return new Promise((resolve) => {
        doc.getInfo(function(err, info) {
          log.info('Get trades');
          tradesSheet = info.worksheets[2];
          clearAndUpdate();
          resolve();
        });
      });
    }).catch(err => log.error('gSheets getTradeSheet', err));
  }
}

module.exports = {
  initSheets,
  sendStatus,
  getPanel,
  updateTradeSheet
}
