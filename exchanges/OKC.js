const OKCoin = require('okcoin');
const util = require('../utils/util.js');
const _ = require('lodash');
const moment = require('moment');
const log = require('../utils/log');

const OKC = function (config) {
  _.bindAll(this);
  if (_.isObject(config)) {
    this.key = config.key;
    this.secret = config.secret;
  }
  this.pair = [config.asset, config.currency].join('_').toLowerCase();
  this.name = 'okcoin';
  this.okcoin = new OKCoin(this.key, this.secret);
  // this.lastTid = false;
};

// if the exchange errors we try the same call again after
// waiting 10 seconds
OKC.prototype.retry = function (method, ...args) {
  const wait = +moment.duration(10, 'seconds');
  log.debug(this.name, 'returned an error, retrying..');

  const self = this;
  const thisOk = this.okcoin;

  // make sure the callback (and any other fn)
  // is bound to OKC
  _.each(args, function(arg, i) {
    if (_.isFunction(arg))
    args[i] = _.bind(arg, self);
  });

  // run the failed method again with the same
  // arguments after wait
  setTimeout(
    function () {
      method.apply(thisOk, args)
    },
    wait
  );
}

// Get free currencies
OKC.prototype.getPortfolio = function (callback) {
  const calculate = function (err, data) {
    if (err) {
      return log.error(`${err.name}: ${err.message} -> Cause: ${err.cause}`);
    }

    const portfolio = {};
    _.each(data.info.funds.free, (amount, asset) => {
      portfolio[asset.toUpperCase()] = +amount;
    });

    callback(err, portfolio);
  }.bind(this);

  return this.okcoin.getUserInfo(calculate);
};


OKC.prototype.getTicker = function (callback) {
  const tickerCall = function (err, data) {
    if (err) {
      console.log('OKC Ticker error', err);
      return this.retry(this.okcoin.getTicker(tickerCall, this.pair));
    }

    const ticker = {
      bid: +data.ticker.buy,
      ask: +data.ticker.sell
    };

    callback(err, ticker);
  }.bind(this);

  this.okcoin.getTicker(tickerCall, this.pair);
};


OKC.prototype.buy = function (rawAmount, price, callback) {
  const amount = Math.floor(rawAmount * 10000) / 10000;
  const set = function (err, result) {
    let error;
    let order;

    if(err || result.OrdRejReason) {
      error = `OKC unable to buy: ${err.name}\n ${err.message}\n\n CAUSE: ${err.cause}\n\n Result: ${JSON.stringify(result, undefined, 2)}`;
    } else {
      order = {
        amount,
        price,
        orderId: result.order_id,
        action: 'buy',
        assets: this.pair
      }
    }

    callback(error, order);
  }.bind(this);

  this.okcoin.addTrade(set, this.pair, 'buy', amount, price);
};

OKC.prototype.sell = function (rawAmount, price, callback) {
  const amount = Math.floor(rawAmount * 10000) / 10000;
  const set = function (err, result) {
    let error;
    let order;

    if(err || result.OrdRejReason) {
      error = `OKC unable to sell: ${err.name}\n ${err.message}\n\n CAUSE: ${err.cause}\n\n Result: ${JSON.stringify(result, undefined, 2)}`;
    } else {
      order = {
        amount,
        price,
        orderId: result.order_id,
        action: 'sell',
        assets: this.pair
      }
    }

    callback(error, order);
  }.bind(this);

  this.okcoin.addTrade(set, this.pair, 'sell', amount, price);
};

OKC.prototype.checkOrder = function (orderId, callback) {
  const check = function (err, result) {
    let orderStatus;

    if(err) {
      log.error('OKC unable to check order:', err, result);
      return { err };
    }

    //-1 = cancelled, 0 = unfilled, 1 = partially filled, 2 = fully filled, 4 = cancel request in process
    if (result.orders.length) {
      const order = result.orders[0];
      const type = order.type;
      orderStatus = {
        type,
        amount: order.amount,
        executedAmount: order.deal_amount,
        price: order.price
      }
    }

    return callback(err, orderStatus);
  };

  this.okcoin.getOrderInfo(check, this.pair, orderId);
};


OKC.prototype.cancelOrder = function (orderId, callback) {
  const cancel = function (err, result) {
    if(err || !result) {
      log.error(`OKC unable to cancel order ${orderId} (`, err.message, result, ')');
    }
    return callback(err, result);
  };

  this.okcoin.cancelOrder(cancel, this.pair, orderId);
};


OKC.prototype.getTrades = function (callback) {
  var args = _.toArray(arguments);
  const tradesCall = function (err, data) {
    if (err) {
      return this.retry(this.getTrades, args);
    }

    const trades = _.map(data, (trade) => {
      return {
        price: +trade.price,
        amount: +trade.amount,
        tid: +trade.tid,
        date: trade.date,
        type: trade.type
      };
    });

    callback(null, trades.reverse());
  }.bind(this);

  this.okcoin.getTrades(tradesCall);
};

OKC.prototype.getOrderbook = function(callback) {
  const orderbook = function (err, response) {
    if (err) {
      return this.retry(this.getOrderbook, args);
    }

    callback(undefined, response);
  }.bind(this);

  this.okcoin.getDepth(orderbook);
}

module.exports = OKC;
