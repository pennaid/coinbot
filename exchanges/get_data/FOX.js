const axios = require('axios');
const { dataKeeper } = require('../../config.json');

// GET PRICES EM BRL
function getPrice () {
  amount = dataKeeper.BTCsToConsider;

  // response pair, bids, asks [unit price, amount, userId]
  return axios.get('https://api.blinktrade.com/api/v1/BRL/orderbook')
  .then((response) => {
    const start = Date.now();
    const orders = response.data;

    let bidValue = 0;
    let bidQty = 0;
    let askValue = 0;
    let askQty = 0;
    // console.log(orders);

    orders.bids.every((bid) => {
      if (bidQty < amount) {
        if (bidQty + bid[1] < amount) {
          bidValue += bid[1] * bid[0];
          bidQty += bid[1];
        } else {
          const need = amount - bidQty;
          bidValue += bid[0] * need;
          bidQty += need;
        }
      } else {
        return false;
      }
      // console.log('BID', amount, ':', bidValue, bidQty);
      return true;
    });

    orders.asks.every((ask) => {
      if (askQty < amount) {
        if (askQty + ask[1] < amount) {
          askValue += ask[1] * ask[0];
          askQty += ask[1];
        } else {
          const need = amount - askQty;
          askValue += ask[0] * need;
          askQty += need;
        }
      } else {
        return false;
      }
      // console.log('ASK', amount, ':', askValue, askQty);

      return true;
    });

    return {
      exchange: 'fox',
      time: Date.now(),
      baseAmount: amount,
      bid: {
        value: bidValue / bidQty,
        amount: 1
      },
      ask: {
        value: askValue / askQty,
        amount: 1
      }
    }

  });
}

module.exports = {
  getPrice
}
