const WebSocket = require('ws');
const log = require('../../utils/log');
const { Broker } = require('../../core/Broker');

const candleChannel24 = 'ok_sub_spotusd_btc_kline_day';
// TODO add Hoff parametrizavel const candleChannelHoff = 'ok_sub_spotusd_btc_kline_' + 
const tickerChannel = 'ok_sub_spotusd_btc_ticker';

const ws = new WebSocket('wss://real.okcoin.com:10440/websocket/okcoinapi');

// Subscribe to channels function, so it can be retried
function subscribeChannel() {
  const subscribes = [{
    event: 'addChannel',
    channel: candleChannel24
  },
  {
    event: 'addChannel',
    channel: tickerChannel
  }];

  ws.send(JSON.stringify(subscribes));
}

// When open, subscribe channels
ws.on('open', () => {
  subscribeChannel();
});

// Event listener
ws.on('message', (data) => {
  const json = JSON.parse(data);

  // Retry subscription on channel after 5s
  if (json[0].success === 'false') {
    log.error(`OKCOIN ${json[0].channel} returned error:`, json[0].errorcode);
    return setTimeout(subscribeChannel, 5000);
  }

  // If there is data, check channel and send to Broker
  if (json[0].data) {
    switch (json[0].channel) {
      case candleChannel24:
        Broker.newCandle('okcoin', json);
        break;
      case tickerChannel:
        Broker.newTicker('okcoin', json);
        break;
    }
  }
});
