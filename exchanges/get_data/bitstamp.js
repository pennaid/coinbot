const Pusher = require('pusher-js/node');
const { Broker } = require('../../core/Broker');

const bitstamp = new Pusher('de504dc5763aeef9ff52');
const channel = bitstamp.subscribe('live_trades');

channel.bind('trade', (ticker) => {
  Broker.newTicker('bitstamp', ticker);
});
