const BitfinexApi = require('bitfinex-api-node');
const log = require('../../utils/log');
const { Broker } = require('../../core/Broker');

const bws = new BitfinexApi().ws;

bws.on('open', () => {
  bws.subscribeTicker('BTCUSD');
});

bws.on('ticker', (pair, ticker) => {
  Broker.newTicker('bitfinex', ticker);
});
