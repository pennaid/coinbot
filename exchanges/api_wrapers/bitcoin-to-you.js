// All @params are CASE INSENSITIVE

var querystring = require("querystring");
var https = require('https');
var _ = require('lodash');
var crypto = require('crypto');

_.mixin({
  // compact for objects
  compactObject: function(to_clean) {
    _.map(to_clean, function(value, key, to_clean) {
      if (value === undefined)
        delete to_clean[key];
    });
    return to_clean;
  }
});

function getMarket(options) {
  if(options) {
    var currency = options.currency && options.currency.toUpperCase() || 'BRL';
    var asset = options.asset && options.asset.toUpperCase() || 'BTC';
  }

  return market = currency + asset || 'BRLBTC';
}

var BitcoinToYou = function(key, secret) {
  this.key = key;
  this.secret = secret;

  _.bindAll(this);
}

BitcoinToYou.prototype._request = function(path, callback, args, headers) {
  var options = {
    host: 'www.bitcointoyou.com',
    path: path,
    method: 'get',
    headers: {
      'User-Agent': 'Mozilla/4.0 (compatible; BitcoinToYou node.js client)'
    }
  };

  if(headers) {
    _.assign(options.headers, headers);
  }

  var req = https.request(options, function(res) {
    res.setEncoding('utf8');
    var buffer = '';
    res.on('data', function(data) {
      buffer += data;
    });
    res.on('end', function() {
      if (res.statusCode !== 200) {
        return callback(new Error('BitcoinToYou error ' + res.statusCode + ': ' + buffer));
      }
      try {
        var json = JSON.parse(buffer);
      } catch (err) {
        return callback(err);
      }

      if (json.success === '0')
        return callback('BitcoinToYou error: ' + json.error);

      callback(null, json);
    });
  });

  req.on('error', function(err) {
    callback(err);
  });

  req.end();
}

// if you call new Date to fast it will generate
// the same ms, helper to make sure the nonce is
// truly unique (supports up to 999 calls per ms).
BitcoinToYou.prototype._generateNonce = function() {
  var now = new Date().getTime();

  if(now !== this.last)
    this.nonceIncr = -1;

  this.last = now;
  this.nonceIncr++;

  // add padding to nonce incr
  // @link https://stackoverflow.com/questions/6823592/numbers-in-the-form-of-001
  var padding =
    this.nonceIncr < 10 ? '000' :
      this.nonceIncr < 100 ? '00' :
        this.nonceIncr < 1000 ?  '0' : '';
  return now + padding + this.nonceIncr;
}

BitcoinToYou.prototype._public = function(action, callback, args) {
  var path = '/API/' + action;

  args = _.compactObject(args);
  path += (querystring.stringify(args) === '' ? '/' : '/?') + querystring.stringify(args);

  this._request(path, callback, args);
}


BitcoinToYou.prototype._private = function(action, callback, args) {
  if(!this.key || !this.secret)
    return callback(new Error('Must provide key, secret and client ID to make this API request.'));

  var path = '/API/' + action;

  var nonce = this._generateNonce();
  var message = nonce + this.key;
  var signer = crypto.createHmac('sha256', new Buffer(this.secret, 'utf8'));
  var signature = signer.update(message).digest('base64').toUpperCase();

  headers = {
    key: this.key,
    signature: signature,
    nonce: nonce
  };

  args = _.compactObject(args);
  path += (querystring.stringify(args) === '' ? '/' : '/?') + querystring.stringify(args);
  this._request(path, callback, args, headers);
}

//
// Public API
//

// @params options object {currency[BRL], asset[BTC, LTC]}
BitcoinToYou.prototype.ticker = function(callback, options) {
  var market = getMarket(options);

  if (market === 'BRLBTC') {
    this._public('ticker.aspx', callback);
  } else if(market === 'BRLLTC') {
    this._public('ticker_litecoin.aspx', callback);
  }
}

BitcoinToYou.prototype.orderBook = function(callback, options) {
  var market = getMarket(options);

  if (market === 'BRLBTC') {
    this._public('orderbook.aspx', callback);
  } else if(market === 'BRLLTC') {
    this._public('orderbook_litecoin.aspx', callback);
  }
}

// Options: {timestamp[Unix time], asset[BTC, LTC], tid[>id]}
BitcoinToYou.prototype.trades = function(callback, options) {
  var args = _.pick(options, ['timestamp', 'asset', 'tid']);
  args.currency = args.asset.toUpperCase();
  delete args.asset;

  this._public('trades.aspx', callback, args);
}

//
// Private API
// (you need to have key / secret / client ID set)
//

BitcoinToYou.prototype.balance = function(callback) {
  this._private('balance.aspx', callback);
}

// Options = {asset[BTC,LTC], action[buy,sell], amount, price} <- ALL REQUIRED
BitcoinToYou.prototype.createOrder = function(callback, options) {
  if (!options.asset && !options.action && !options.amount && !options.price) {
    return callback(new Error('Must provide asset[BTC,LTC], action[buy,sell], amount, price to make this API request.'));
  }

  options.asset = options.asset.toUpperCase();
  options.action = options.action.toLowerCase();

  this._private('createorder.aspx', callback, options);
}

// @param id number order ID to cancel
BitcoinToYou.prototype.cancelOrder = function(callback, id) {
  var args = { id: id };

  this._private('deleteorders.aspx', callback, args);
}

// @param id number order ID to retrieve
BitcoinToYou.prototype.getOrder = function (callback, id) {
  var args = { id: id };

  this._private('getordersid.aspx', callback, args);
};

// @param status string OPEN, CANCELED, EXECUTED
BitcoinToYou.prototype.getOrdersByStatus = function (callback, status) {
  var args = { status: status.toUpperCase() };

  this._private('getorders.aspx', callback, args);
};

module.exports = BitcoinToYou;
