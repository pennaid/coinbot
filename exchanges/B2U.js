var BitcoinToYou = require('./api_wrapers/bitcoin-to-you');
var _ = require('lodash');
var moment = require('moment');
var log = require('../utils/log');

var B2U = function(config) {
  _.bindAll(this);
  if(_.isObject(config)) {
    this.key = config.key;
    this.secret = config.secret;
    this.asset = config.asset;
    this.currency = config.currency;
    this.market = (this.asset + this.currency).toUpperCase();
  }
  this.name = 'BitcoinToYou';

  this.btcToYou = new BitcoinToYou(this.key, this.secret);
}

// if the exchange errors we try the same call again after
// waiting 10 seconds
B2U.prototype.retry = function(method, args) {
  var wait = +moment.duration(10, 'seconds');
  log.debug(this.name, 'returned an error, retrying..');

  var self = this;

  // make sure the callback (and any other fn)
  // is bound to B2U
  _.each(args, function(arg, i) {
    if(_.isFunction(arg))
    args[i] = _.bind(arg, self);
  });

  // run the failed method again with the same
  // arguments after wait
  setTimeout(
    function() { method.apply(self, args) },
    wait
  );
}

B2U.prototype.getRequestInterval = function () {
  return this.requestInterval;
}

B2U.prototype.getPortfolio = function(callback) {
  var set = function(err, data) {

    if(_.isEmpty(data) || err)
      return callback('BITCOINTOYOU API ERROR: portfolio is empty or there is an error: ' + err);

    var portfolio = {};
    _.each(data.oReturn[0], function(amount, asset) {
      if (asset === 'OrderBuyPending') {
        asset = 'BRL_locked';
      } else if (asset === 'OrderSellBTCPending') {
        asset = 'BTC_locked';
      }
      portfolio[asset] = parseFloat(amount);
    });
    callback(err, portfolio);
  }.bind(this);

  this.btcToYou.balance(set);
}

B2U.prototype.getTicker = function(callback) {

  var set = function(err, data) {
    if(_.isEmpty(data))
    return callback('BITCOINTOYOU API ERROR: ' + data.error);

    var ticker = {bid: data.ticker.buy, ask: data.ticker.sell};

    callback(err, ticker);
  }

  var options = {
    currency: this.currency,
    asset: this.asset
  }

  this.btcToYou.ticker(set, options);
}

B2U.prototype.buy = function(amount, price, callback) {
  var set = function(err, result) {
    let error;
    let order;

    if(err || result.error) {
      error = `B2U unable to buy: ${err} ${JSON.stringify(result, undefined, 2)}`;
    } else {
      const order = {
        amount,
        price,
        orderId: result.oReturn[0].id,
        action: 'buy',
        assets: this.market
      }
    }

    callback(error, order);
  }.bind(this);

  // prevent: Ensure that there are no more than 8 digits in total.
  amount *= 100000000;
  amount = Math.floor(amount);
  amount /= 100000000;

  // prevent:
  // 'Ensure that there are no more than 2 decimal places.'
  price *= 100;
  price = Math.floor(price);
  price /= 100;

  var options = {
    asset: this.asset,
    action: 'buy',
    amount: amount,
    price: price //the price is set for 1B, not for the order
  }

  this.btcToYou.createOrder(set, options);
}

B2U.prototype.sell = function(amount, price, callback) {
  var set = function(err, result) {
    let error;
    let order;

    if(err || result.error) {
      error = `B2U unable to sell: ${err} ${JSON.stringify(result, undefined, 2)}`;
    } else {
      order = {
        amount,
        price,
        orderId: result.oReturn[0].id,
        action: 'sell',
        assets: this.market
      }
    }

    callback(error, order);
  }.bind(this);

  // prevent:
  // 'Ensure that there are no more than 2 decimal places.'
  price *= 100;
  price = Math.ceil(price);
  price /= 100;

  var options = {
    asset: this.asset,
    action: 'sell',
    amount: amount,
    price: price
  }

  this.btcToYou.createOrder(set, options);
}

// check if order is still in process
B2U.prototype.checkOrder = function(order, callback) {
  var check = function(err, result) {
    let status;

    if(err) {
      log.error('unable to check order:', err, result);
    } else {
      const order = result.oReturn[0];

      const isOpen = order.status === 'open';
      const isPartial = order.amount - order.executedAmount > 0;

      const isActive = isOpen || isPartial;

      status = {
        isActive,
        amount: order.amount,
        executedAmount: order.executedAmount,
        type: order.action,
        price: order.executedPriceAverage
      }
    }

    callback(err, status);
  }.bind(this);

  this.btcToYou.getOrder(check, order);
}

B2U.prototype.cancelOrder = function(order, callback) {
  var cancel = function(err, result) {
    if(err || !result) {
      log.error('unable to cancel order', order, '(', err, result, ')');
    }
    callback(result);
  }.bind(this);

  this.btcToYou.cancelOrder(cancel, order);
}

B2U.prototype.getTrades = function(callback) {
  var args = _.toArray(arguments);
  var process = function(err, trades) {
    if(err) {
      return this.retry(this.getTrades, args);
    }

    var result = _.map(trades, t => {
      return {
        date: t.date,
        tid: +t.tid,
        price: +t.price,
        amount: +t.amount
      }
    })

    callback(null, result.reverse());
  }.bind(this);

  this.btcToYou.trades(process, {asset: this.asset});
}

module.exports = B2U;
