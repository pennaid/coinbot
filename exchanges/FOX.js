var BlinkTradeWS = require("blinktrade").BlinkTradeWS;
var BlinkTradeRest = require("blinktrade").BlinkTradeRest;

var _ = require('lodash');
var moment = require('moment');
var log = require('../utils/log');

var Fox = function(config) {
  _.bindAll(this);
  if(_.isObject(config)) {
    this.key = config.key;
    this.secret = config.secret;
    this.currency = config.currency;
    this.pair = (config.asset + config.currency).toUpperCase();
    this.password = config.password;
    this.amountToQuote = config.amountToQuote;
  }
  this.name = 'FOX';

  var options = {
    prod: true,
    brokerId: 4
  }

  var auth = {
    username: this.key,
    password: this.password,
  }

  this.connected = false;
  this.logged = false;

  this.foxbit = new BlinkTradeWS(options);
  this.foxbit.connect()
  .then(function() {
    this.connected = true;
    return this.foxbit.login(auth);
  }.bind(this))
  .then(function(logged) {
    log.info('Logged in Foxbit.');

    // REST endpoint for Last Trades
    this.foxbitRest = new BlinkTradeRest({ currency: this.currency });
    return this.logged = true;
  }.bind(this))
  .catch(e => log.info(e));
}

// For Async. Check if websocket is connected or logged so methods may trigger
// @param mode string connected / logged
function holdStatus (method, arguments) {
  var wait = +moment.duration(1, 'seconds');
  // log.debug(this.name, 'returned an error, retrying..');

  var self = this;
  var args = _.toArray(arguments)

  // make sure the callback (and any other fn)
  // is bound to Fox
  _.each(args, function(arg, i) {
    if(_.isFunction(arg))
    args[i] = _.bind(arg, self);
  });

  // run the failed method again with the same
  // arguments after wait
  setTimeout(
    function() { method.apply(self, args) },
    wait
  );
}

// if the exchange errors we try the same call again after
// waiting 10 seconds
Fox.prototype.retry = function(method, args) {
  var wait = +moment.duration(10, 'seconds');
  log.debug(this.name, 'returned an error, retrying..');

  var self = this;

  // make sure the callback (and any other fn)
  // is bound to Fox
  _.each(args, function(arg, i) {
    if(_.isFunction(arg))
    args[i] = _.bind(arg, self);
  });

  // run the failed method again with the same
  // arguments after wait
  setTimeout(
    function() { method.apply(self, args) },
    wait
  );
}

Fox.prototype.getRequestInterval = function () {
  return this.requestInterval;
}

Fox.prototype.getPortfolio = function(callback) {
  var set = function(err, data) {
    if (err)
    return log.error('unable to get portfolio:', err, '\nDATA:', data);

    if(data) {

      var portfolio = {};
      _.each(data['4'], function(amount, asset) {
        portfolio[asset] = parseFloat(amount / 1e8);
      });

      callback(err, portfolio);
    }
  }.bind(this);

  if(!this.logged)
    return holdStatus.call(this, arguments.callee, arguments);

  this.foxbit.balance(set)
  .catch(function(err) { return log.info('Promise error: ', err) });
}

Fox.prototype.getTicker = function(callback) {

  var set = function(err, data) {
    if(_.isEmpty(data))
    return callback('FOXBIT API ERROR: ' + data.error);

    var ticker = {bid: data.BestBid, ask: data.BestAsk};

    callback(err, ticker);
  }

  if(!this.connected)
  return holdStatus.call(this, arguments.callee, arguments);

  this.foxbit.subscribeTicker([this.pair], set);
}

Fox.prototype.buy = function(amount, price, callback) {
  var set = function(err, result) {
    let error;
    let order;

    if(err || result.OrdRejReason) {
      error = `FOX unable to buy: ${err} ${JSON.stringify(result, undefined, 2)}`;
    } else {
      order = {
        amount,
        price,
        orderId: result.OrderID,
        action: 'buy',
        assets: this.pair
      }
    }

    callback(error, order);
  }.bind(this);

  // prevent: Ensure that there are no more than 8 digits in total.
  amount *= 100000000;
  amount = Math.floor(amount);
  amount /= 100000000;

  // prevent:
  // 'Ensure that there are no more than 2 decimal places.'
  price *= 100;
  price = Math.floor(price);
  price /= 100;

  var order = {
    side: '1',
    price: parseInt((price * 1e8).toFixed(0)),
    amount: parseInt((amount * 1e8).toFixed(0)),
    symbol: this.pair,
    brokerID: 4
  }

  if(!this.logged)
    return holdStatus.call(this, arguments.callee, arguments);

  this.foxbit.sendOrder(order, set);
}

Fox.prototype.sell = function(amount, price, callback) {
  var set = function(err, result) {
    let error;
    let order;

    if(err || result.OrdRejReason) {
      error = `FOX unable to sell: ${err} ${JSON.stringify(result, undefined, 2)}`;
    } else {
      order = {
        amount,
        price,
        orderId: result.OrderID,
        action: 'sell',
        assets: this.pair
      }
    }

    callback(error, order);
  }.bind(this);

  // prevent:
  // 'Ensure that there are no more than 2 decimal places.'
  price *= 100;
  price = Math.ceil(price);
  price /= 100;

  var order = {
    side: '2',
    price: parseInt((price * 1e8).toFixed(0)),
    amount: parseInt((amount * 1e8).toFixed(0)),
    symbol: this.pair,
    brokerID: 4
  }

  if(!this.logged)
    return holdStatus.call(this, arguments.callee, arguments);

  this.foxbit.sendOrder(order, set);
}

Fox.prototype.checkOrder = function(order, callback) {
  var check = function(err, result) {
    let orderStatus;
    let foundOrder;

    if(err) {
      log.error('FOX unable to check order:', err, result);
      return { err };
    } else {
      foundOrder = _.find(result.OrdListGrp, function(obj) {
        //“0” = New, “1” = Partially filled, “2” = Filled, “4” = Cancelled, “8” = Rejected, “A” = Pending New
        if (obj.OrderID === order && (parseInt(obj.OrdStatus) < 2 || obj.OrdStatus === 'A')) {
          return true;
        }
      });
    }

    if (foundOrder) {
      const type = foundOrder.Side == 1 ? 'buy' : 'sell';
      orderStatus = {
        type,
        amount: foundOrder.OrderQty / 1e8,
        executedAmount: foundOrder.CumQty / 1e8,
        price: foundOrder.Price / 1e8
      }
    }

    // If order not found and there might be more orders, request more orders
    if (!orderStatus && result.OrdListGrp.length === 40) {
      this.foxbit.myOrders({ page: arguments[0].page++ }, check);
    } else {
      return callback(err, orderStatus);
    }

  }.bind(this);

  if(!this.logged)
    return holdStatus.call(this, arguments.callee, arguments);

  this.foxbit.myOrders({ page: 0 }, check);
}

Fox.prototype.cancelOrder = function(orderId, callback) {
  var cancel = function(err, result) {
    console.log('canceled');
    if(err || !result) {
      log.error(`unable to cancel order ${orderId} (`, err, result, ')');
    }
    return callback(result);
  }.bind(this);

  if(!this.logged) {
    return holdStatus.call(this, arguments.callee, arguments);
  }

  this.foxbit.cancelOrder({ orderId: orderId }, cancel);
}

Fox.prototype.getTrades = function(callback) {
  var args = _.toArray(arguments);
  var tradesCall = function(err, trades) {
    if(err) {
      return this.retry(this.getTrades, args);
    }

    var result = _.map(trades, t => {
      return {
        date: t.date,
        tid: +t.tid,
        price: +t.price,
        amount: +t.amount
      }
    })

    callback(null, result);
  }.bind(this);

  // let this to don't block the stack flow
  if(!this.logged)
  return holdStatus.call(this, arguments.callee, arguments);

  this.foxbitRest.trades(undefined, tradesCall);
}

Fox.prototype.getOrderbook = function(callback) {
  const process = function (response) {
    const orders = response.data;
    const amount = this.amountToQuote;

    let bidValue = 0;
    let bidQty = 0;
    let askValue = 0;
    let askQty = 0;

    orders.bids.every((bid) => {
      if (bidQty < amount) {
        if (bidQty + bid[1] < amount) {
          bidValue += bid[1] * bid[0];
          bidQty += bid[1];
        } else {
          const need = amount - bidQty;
          bidValue += bid[0] * need;
          bidQty += need;
        }
      } else {
        return false;
      }
      return true;
    });

    orders.asks.every((ask) => {
      if (askQty < amount) {
        if (askQty + ask[1] < amount) {
          askValue += ask[1] * ask[0];
          askQty += ask[1];
        } else {
          const need = amount - askQty;
          askValue += ask[0] * need;
          askQty += need;
        }
      } else {
        return false;
      }
      return true;
    });

    return {
      exchange: 'FOX',
      time: Date.now(),
      amount,
      bid: {
        value: bidValue,
        amount: bidQty
      },
      ask: {
        value: askValue,
        amount: askQty
      }
    }
  }.bind(this);

  // let this to don't block the stack flow
  if(!this.logged)
    return holdStatus.call(this, arguments.callee, arguments);

  this.foxbitRest.orderbook(undefined, process);
}


module.exports = Fox;
