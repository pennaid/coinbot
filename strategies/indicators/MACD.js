// required indicators
const EMA = require('./EMA.js');

class MACD {
  constructor(config) {
    this.diff = false;
    this.short = new EMA(config.short);
    this.long = new EMA(config.long);
  }

  update (prices) {
    this.short.update(prices);
    this.long.update(prices);
    this.calculateEMAdiff();
  }

  calculateEMAdiff () {
    var shortEMA = this.short.result;
    var longEMA = this.long.result;

    this.emaShort = shortEMA;
    this.emaLong = longEMA;

    this.diff = shortEMA - longEMA;
  }
}

module.exports = MACD;
