
class RSI {
  constructor (periods) {
    this.periods = periods;
  }

  calculateRSI () {
    const rs = this.getRS();

    if (!rs) this.value = 100;
    this.value = 100 - (100 / (1 + rs));
  }

  calculateDiff () {
    let priceLast;
    let priceDiff = [];

    this.prices.forEach((price) => {
      if (!priceLast) {
        priceLast = price;
      } else {
        priceDiff.push(price - priceLast);
      }
    });

    return priceDiff;
  }

  getRS () {
    const priceDiff = this.calculateDiff();
    const RSIperiods = this.periods;
    let loss;
    let gain;

    if (priceDiff.length >= RSIperiods) {
      for (let i = 0; i < RSIperiods; i++) {
        if (priceDiff[i] <= 0) {
          loss -= priceDiff[i];
        } else {
          gain += priceDiff[i];
        }
      }

      const avgGain = gain / RSIperiods;
      const avgLoss = loss / RSIperiods;

      if (!avgLoss) return 0;

      this.rs = avgGain / avgLoss;
    }
  }

  updateRSI (prices) {
    this.prices = prices;
    this.calculateRSI();
  }
}

module.exports = RSI;
