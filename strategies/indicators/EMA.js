const config = require('../../config.json').indicators;

// const prices = [22.81, 23.09, 22.91, 23.23,22.83,23.05,23.02,23.29];


class EMA {
  constructor (periods) {
    this.periods = periods;
  }

  calculate (price, k, emayday) {
    return price * k + emayday * (1-k);
  }

  update (prices) {
    let emas = [];
    let i = 1;
    emas[0] = prices[0];

    while (i < prices.length && i <= this.periods) {
      const k = 2 / (i + 1);
      const emayday = emas[i - 1];
      emas[i] = this.calculate(prices[i], k, emayday);

      i++;
    }

    this.result = emas[i - 1];
  }
}

module.exports = EMA;
