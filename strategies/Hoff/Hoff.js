const { errorOccurrence } = require('../../utils/errorOccurrence');

const config = require('../../config.json');
const log = require('../../utils/log');
const RSI = require('../indicators/RSI');
const MACD = require('../indicators/MACD');

class Hoff {
  constructor (dataKeeper, trader) {
    this.dataKeeper = dataKeeper;
    this.trader = trader;
    this.macd = new MACD(config.hoff.macd);
    this.rsi = new RSI(config.hoff.rsiPeriods);

    this.runStrategy();
    // setInterval(this.runStrategy.bind(this), config.hoff.refreshPosition);
    setInterval(this.runStrategy.bind(this), 10000);
  }

  getPrices () {
    let prices = [];

    this.dataKeeper.candles.forEach((candle) => {
      prices.push(candle.close);
    });

    this.prices = prices;
  }

  order() {
    function sendOrder(side) {
      this.side = side;
      return this.getOrderPrice(side).then(() => {
        console.log('amount:', this.amount);
        console.log('price:', this.price);

        this.amount = 0.01;
        this.price = 10000;
        console.log(this.side);

        return this.trader[this.side]('OKC', this.amount, this.price).then((data) => {
          console.log(data);
          this.orderId = data.id;
          this.currentPosition = side;
        });
      });
    }

    let side;

    const limitMACD = config.hoff.macd.limit * this.macd.emaShort;

    if (this.macd.diff >= limitMACD && this.rsi.value < 40) {
      side = 'buy';
    } else if (this.macd.diff < (-limitMACD) && this.rsi.value > 60) {
      side = 'sell';
    }
side = 'sell';
    if (!side) return;
console.log('currentPosition', this.currentPosition)
console.log('orderid', this.orderId)
    if (this.currentPosition == side && this.orderId) {
      return sendOrder.call(this, side);
    } else {
      this.getOrderAmount(side).then(() => {
        return sendOrder.call(this, side);
      });
    }
  }

  runStrategy() {
    this.refreshRSI();
    this.refreshMACD();


    if (!this.orderId) {
      // TODO get open orders getOrderInfo(console.log, 'btc_usd', '-1')
      // https://www.npmjs.com/package/okcoin
      // setar o id da order aberta como orderId, e se não tiver continua como se fosse primeira ordem
    }

    if (this.orderId) {
      // Cancel order before sending
      this.trader.checkOrder('OKC', this.orderId).then((data) => {
        if (data.executedAmount < data.amount) {
          this.amount = data.amount - data.executedAmount;
          this.trader.cancelOrder('OKC', orderId).then(() => {
            this.order();
          });
        } else {
          this.order();
        }
      });
    } else {

      this.order();
    }
  }

  getOrderAmount (side) {
    let asset;
    if (side == 'buy') {
      asset = 'USD';
    } else {
      asset = 'BTC';
    }

    return this.trader.getPortfolio('OKC').then((data) => {
      this.amount = data[asset];
    });
  }

  getOrderPrice (side) {
    return this.trader.getOrderbook('OKC').then((data) => {
      const orders = data;
      const amount = this.amount; // might be USD (buy) or BTC (sell)
      let value = 0;
      let quantity = 0;

      if (side == 'buy') {
        let i = 0;
        orders.bids.every((order) => {
          if (quantity < amount) {
            quantity += order[0] * order[1];
            value += order[0];
            i++;
          } else {
            return false
          }
          return true;
        });

        value = (value / i) * (1 + config.hoff.priceMargin);

      } else {
        orders.asks.every((order) => {
          if (quantity < amount) {
            if (quantity + order[1] < amount) {
              value += order[1] * order[0];
              quantity += order[1];
            } else {
              const need = amount - quantity;
              value += order[0] * need;
              quantity += need;
            }
          } else {
            return false;
          }
          return true;
        });
      }

      this.price = value * (1 - config.hoff.priceMargin);

    });
  }

  refreshRSI () {
    this.getPrices();
    this.rsi.updateRSI(this.prices);
  }

  refreshMACD () {
    this.getPrices();
    this.macd.update(this.prices);
  }
}

module.exports = Hoff;
