const Card = require('../../db/models/Card');

class HandleCards {
  constructor(sheets) {
    this.cardSheet = sheets.cardSheet;
  }

  updateCard (id, card) {
    card.save(); // save to Google Sheets
    return Card.updateCard(id, card);
  }
}

module.exports = {
  HandleCards
}

// init: (cardSheetParam) => {
// busca cards no g sheets, salva no banco e retorna os cards

// sheets
//   name: 'BitcoinToYou-1',
//   exc: 'B2U',
//   spread: 0.038,                // fixo
//
//   mean: -0.0252,                // conta interna
//   // order
//   buyOrderID: '2474083',        // resposta do gekko
//   sellOrderID: '22084108',      // resposta do gekko
//   buyOrderPrice: 2661.03465999, // resposta do gekko
//   sellOrderPrice: 2951.90573,   // resposta do gekko
//   buyOrderAmount: 0.2951,       //  resposta do gekko
//   sellOrderAmount: 0.2951,      // resposta do gekko
//   totalBuyAmount: 0.25,         // docs + DB
//   totalSellAmount: 0.25,        // docs + DB
//
//
//   maxDiff: 8.5237               // interna
// },
// {

// const cards = [{
//   name: 'BitcoinToYou-2',
//   exc: 'FOX',
//   mean: -0.0252,                // conta interna
//   spread: 0.038,                // docs
//   buyOrderID: 123,              // resposta do gekko
//   sellOrderID: 123,             // resposta do gekko
//   buyOrderPrice: 0,             // resposta do gekko
//   sellOrderPrice: 0,            // resposta do gekko
//   buyOrderAmount: 0,            // resposta do gekko
//   sellOrderAmount: 0,           // resposta do gekko
//   totalBuyAmount: 0.001,          // docs + DB
//   totalSellAmount: 0.001,           // docs + DB
//   maxDiff: 8.5237               // interna
// }];
//
// return Card.removeAllCards()
// .then(() => {
//   return Card.insertManyCards(cards)
//   .then((insertedCards) => {
//     return insertedCards;
//   });
// })
// },
