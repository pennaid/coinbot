const { errorOccurrence } = require('../../utils/errorOccurrence');

const math = require('mathjs');
const config = require('../../config.json');
const log = require('../../utils/log');
const { HandleCards } = require('./handleCards');
const MACD = require('../indicators/MACD');

const Brant = function (dataKeeper, sheets, trader) {

  // Stop loop and END process when everything is done
  errorOccurrence.on('stopProcesses', () => {
    clearTimeout(this.loopTimeout);
  });

  this.dataKeeper = dataKeeper;
  this.trader = trader;
  this.sheets = sheets;

  this.handleCards = new HandleCards(sheets);
  this.cardsArray = sheets.cards;

  //timeToRefreshCard is in days
  const minuteInDay = (1 / 24) / 60; // get time in days (What is 1 minute in 1 day?)
  this.timeToRefreshCard = minuteInDay * (config.cards.refreshTime / 60000);
  this.requestInterval = trader.requestInterval; //TODO requestInterval working?

  this.target = config.brant.target;
  this.memCorretor = 0;
  this.macd = new MACD(config.brant.macd);

  //TODO Loop Update
  this.usdBtc = this.getUsdBtc();
  this.brlUsd = this.getUsdBrl();
  this.target = this.updateTarget(this.target, this.memCorretor) || 0.075;
  this.spread = this.updateSpread() || 0.0096;
  this.brlBtcReal = this.getBrlBtcReal();

  this.brlBtcTeorico = this.brlUsd * this.usdBtc;
  this.agioBR = (this.brlBtcReal - this.brlBtcTeorico) / this.brlBtcTeorico;
  this.custoBR = math.mean(this.target, this.target, this.agioBR);
  this.idealBuy = this.brlBtcTeorico * (1 + this.custoBR) * (1 - this.spread);
  this.idealSell = this.brlBtcTeorico * (1 + this.custoBR) * (1 - this.spread);

  function doLoop() {
    this.loopCards(this.cardsArray).then((newCards) => {
      this.cardsArray = newCards;

      this.loopTimeout = setTimeout(() => {
        this.startTime = Date.now();
        doLoop.call(this);
      }, (this.startTime + config.cards.refreshTime) - Date.now());
    }).catch(e => log.error('Brant loopCards', e));
  }

  // Initialize startTime
  this.startTime = Date.now();
  doLoop.call(this);
};

// cards-loop
Brant.prototype.loopCards = function (cardsArray) {
  const promiseArray = [];
  errorOccurrence.emit('running');

  cardsArray.forEach((card) => {
    promiseArray.push(this.processCard(card));
  });

  // When everything is done, tell cards are done. Only stop COINBOT if there are no cards being processed
  return Promise.all(promiseArray).then((newCardsArray) => {
    errorOccurrence.emit('done');
    return newCardsArray;
  });
};

Brant.prototype.processCard = function (card) {
  // If exchange is not configured to operate
  if (!card || config.exchanges.indexOf(card.exc) < 0) return;

  log.info(`Brant strategy: Start processing ${card.exc} Card ${card.name}`);

  this.updateCardMean(card);

  card.idealBuy = this.idealBuy * (1 + card.mean) * (1 - card.spread);
  card.idealSell = this.idealSell * (1 + card.mean) * (1 + card.spread);

  // -- To cancel outdated orders
  card.maxDiff = (card.idealBuy * (this.spread + card.spread)) / 3;

  const promiseArray = [];
  // BUY PROCESS
  promiseArray.push(
    // -- Check if the orders were executed
    this.trader.checkOrder(card.exc, card.buyOrderID).then((data) => {
      if (!data) {
        // -- If order not found, reset sellOrder
        card.buyOrderID = '';
        card.buyOrderAmount = 0;
        card.availableBuy = card.totalBuyAmount;
      } else {

        if (data.error) {
          throw new Error (data.error);
        }
        // If 'data.amount' is undefined, the order wasn't found and all buy amount is available
        if (data.amount && data.amount - data.executedAmount !== card.buyOrderAmount) {
          const btcExecutedNow = card.buyOrderAmount - (data.amount - data.executedAmount);
          card.buyOrderAmount = data.amount - data.executedAmount;
          card.totalBuyAmount -= btcExecutedNow;
          card.totalBuyAmount += btcExecutedNow;
        }

        // -- Check available amounts
        card.availableBuy = card.totalBuyAmount - card.BuyOrderAmount;

        // Update card with order status
        this.handleCards.updateCard(card._id, card);

        // -- Cancel outdated orders
        if (card.buyOrderID && (math.abs(card.idealBuy - card.buyOrderPrice) > card.maxDiff || math.abs(card.availableBuy) > config.brant.minimumBuy)) {

          return this.trader.cancelOrder(card.exc, card.buyOrderID).then((data) => {
            card.buyOrderID = '';
            card.buyOrderAmount = 0;
            card.availableBuy = card.totalBuyAmount;

            // Update card with cancelation
            this.handleCards.updateCard(card._id, card);
          });
        }
      }

    }).then(() => {
      // -- Create new orders
      if (!card.buyOrderID && card.availableBuy > config.brant.minimumBuy) {
        return this.trader.buy(card.exc, card.availableBuy, card.idealBuy).then((data) => {
          if (data.error) {
            card.buyOrderID = 0;
            card.buyOrderPrice = 0;
            card.buyOrderAmount = 0;
            card.availableBuy = card.totalBuyAmount;
          } else {
            card.buyOrderID = data.id;
            card.buyOrderPrice = data.price;
            card.buyOrderAmount = data.amount;
            card.availableBuy = card.totalBuyAmount - card.buyOrderAmount;
          }

          // Update card with new order
          this.handleCards.updateCard(card._id, card).catch(e => console.log(e));
          return card;

        });
      } // end if
      return card;
    })
  );

  // SELL PROCESS
  promiseArray.push(
    // -- Check if the orders were executed
    this.trader.checkOrder(card.exc, card.sellOrderID).then((data) => {

      if (!data) {
        // -- If order not found, reset sellOrder
        card.sellOrderID = '';
        card.sellOrderAmount = 0;
        card.availableSell = card.totalSellAmount;
      } else {

        if (data.error) {
          new Error(data.err);
        }
        // If 'data.amount' is undefined, the order wasn't found and all buy amount is available
        if (data.amount && data.amount - data.executedAmount !== card.sellOrderAmount) {
          const btcExecutedNow = card.sellOrderAmount - (data.amount - data.executedAmount);
          card.sellOrderAmount = data.amount - data.executedAmount;
          card.totalSellAmount -= btcExecutedNow;
          card.totalBuyAmount += btcExecutedNow;
        }

        // -- Check available amounts
        card.availableSell = card.totalSellAmount - card.SellOrderAmount;

        // Update card with order status
        this.handleCards.updateCard(card._id, card);

        // -- Cancel outdated orders
        if (card.sellOrderID && (math.abs(card.idealSell - card.sellOrderPrice) > card.maxDiff || math.abs(card.availableSell) > config.brant.minimumSell)) {

          return this.trader.cancelOrder(card.exc, card.sellOrderID).then((data) => {
            card.sellOrderID = '';
            card.sellOrderAmount = 0;
            card.availableSell = card.totalSellAmount;

            // Update card with cancelation
            this.handleCards.updateCard(card._id, card);
          });
        }
      }
    }).then(() => {
      // -- Create new orders
      if (!card.sellOrderID && card.availableSell > config.brant.minimumSell) {
        return this.trader.sell(card.exc, card.availableSell, card.idealSell).then((data) => {
          if (data.error) {
            card.sellOrderID = 0;
            card.sellOrderPrice = 0;
            card.sellOrderAmount = 0;
            card.availableSell = card.totalSellAmount;
          } else {
            card.sellOrderID = data.orderId;
            card.sellOrderPrice = data.price;
            card.sellOrderAmount = data.amount;
            card.availableSell = card.totalSellAmount - card.sellOrderAmount;
          }

          // Update card with new order
          this.handleCards.updateCard(card._id, card);
          return card;
        });
      } // end if
      return card;
    })
  );

  return Promise.all(promiseArray).then(function () {
    log.info('Brant -> finish Promise.all');
    return card;
  }).catch(e => console.log(e));
};

Brant.prototype.getUsdBtc = function () {
  return this.dataKeeper.priceUSD.price;
};

Brant.prototype.getUsdBrl = function () {
  return this.dataKeeper.currencies.assets.BRL;
};

Brant.prototype.getPrices = function () {
  let prices = [];

  this.dataKeeper.candles.forEach((candle) => {
    prices.push(candle.close);
  });

  return prices;
}

Brant.prototype.updateTarget = function (targetParam, memCorretor) {

  // tempoAtualizacaoCardGeral está em dias
  const prices = this.getPrices();
  const weight = 0.01; //Força de venda/compra em %
  const targetMax = 0.3;
  const targetMin = 0.0;
  let target = targetParam;

  this.macd.update(prices);
  const limitMACD = config.brant.macd.limit * this.macd.emaShort; //Limite pra operar o MACD
  if (this.macd.diff > limitMACD) {
    if (memCorretor == 0) {
      memCorretor = 1;
      target += weight;
    }
  }
  else if (this.macd.diff < limitMACD) {
    if (memCorretor == 0) {
      memCorretor = -1;
      target -= weight;
    }
  }
  else {
    if (memCorretor == 1) {
      memCorretor = 0;
      target -= weight;
    }
    else {
      memCorretor = 0;
      target += weight;
    }
  }


  if (target * (1 + memCorretor * weight) < this.agioBR) {
    target = target + this.timeToRefreshCard / 100;
    if (target > targetMax) {
      target = targetMax;
    }
  } else {
    target = target - this.timeToRefreshCard / 100
    if (target < targetMin) {
      target = targetMin;
    }
  }

  this.target = target;
  this.memCorretor = memCorretor;

  this.sheets.configCells[53]._numericValue = target;
  this.sheets.configCells[53].save(() => {});
};

Brant.prototype.updateSpread = function () {
  return 0.0075 + math.abs(this.custoBR - this.agioBR)/3
};

Brant.prototype.getBrlBtcReal = function () {
  return this.dataKeeper.priceBRL;
};

Brant.prototype.updateCardMean = function (card) {
  if (card.totalBuyAmount + card.totalSellAmount > 0){
    const porcBTC = card.totalSellAmount / (card.totalBuyAmount + card.totalSellAmount);
    const porcDesejada = 0.1;

    card.mean += (1 - porcBTC / porcDesejada) * this.timeToRefreshCard / 100
  }
};

module.exports = Brant;
